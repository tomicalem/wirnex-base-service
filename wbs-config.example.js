// Wirnex Base Service Config
const pack = require('./package.json');

// Here is four string!
module.exports = {
  name: pack.name,
  version: pack.version,
  entityNamePlural: 'candies',
  entityNamePluralUcfirst: 'Cadies',
};
