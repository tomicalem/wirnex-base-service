# rabbit-exchange


rabbit-config.js sample
```js
{
  queuesSubscribe: [{
      name: 'CampaignQuery',
      key: ($) ? 'campaignQuery' : 'testCampaignQuery',
      sources: [{
        exchange: { name: 'amq.direct', type: 'direct' },
        routingKeys: ['advertiser.create', 'advertiser.update', 'advertiser.delete'],
      }],
  }],
}
```

Usage:
```js
rabbit.consumeFromSubscribe('CampaignQuery', (msg, channel) => {
  console.log(`received message from ${msg.fields.routingKey} with content ${msg.content.toString()}`);
  switch (msg.fields.routingKey) {
    case 'advertiser.create':
      patchResource('advertiser', msg, channel);
      break;
    case 'advertiser.update':
      patchResource('advertiser', msg, channel);
      break;
    case 'advertiser.delete':
      destroyResource('advertiser', msg, channel);
      break;
    default:
      break;
  }
});
```


rabbit-config.js sample
```js
{
    actionsPublish: {
        update: {
            exchange: 'Direct',
            routingKey: 'advertiser.update',
        },
        create: {
            exchange: 'Direct',
            routingKey: 'advertiser.create',
        },
        destroy: {
            exchange: 'Direct',
            routingKey: 'advertiser.destroy',
        },
    },
    
    exchanges: [{
        name: 'Direct',
        key: 'amq.direct',
        type: 'direct',
    }],
}
```


Usage:
```js
rabbit.publishToExchange({
    key: 'value',
}, 'update');
```
