const test = process.env.NODE_ENV === 'test';

module.exports = test ?
  require('./rabbit-config.js') :
  require('rabbit-config.js');

