const amqp = require('amqplib');
const http = require('http');
const rabbitConfig = require('./config');

class Helper {

  /**
   * @param {Object} state
   */
  constructor(state) {
    this.state = state;
  }

  /**
   * @public
   * @returns {*}
   */
  contentToBuffer(content) {
    return (typeof content === 'object')
      ? new Buffer(JSON.stringify(content))
      : new Buffer(content);
  }

  /**
   * @public
   * @returns {*}
   */
  generateConfiguration(host, port) {
    if (!host) host = process.env.RABBITMQ_HOST;
    if (!port) port = 5672;
    this.state.config = {
      protocol: (process.env.RABBITMQ_SSL_ENABLED) ? 'amqps://' : 'amqp://',
      user: process.env.RABBITMQ_DEFAULT_USER || null,
      pass: process.env.RABBITMQ_DEFAULT_PASS || null,
      host: host,
      port: port,
      adminPort: process.env.RABBITMQ_ADMIN_PORT || 15672,
      maxConcurrentJobs: null,
      queues: [],
      exchanges: [],
      policies: [],
      upstreams: [],
      queuesSubscribe: [],
    };

    this.state.config.url = `${this.state.config.protocol}${this.state.config.user}:${this.state.config.pass}@${this.state.config.host}`;

    Object.keys(rabbitConfig).reduce((acc, key) => {
      acc[key] = rabbitConfig[key];

      return acc;
    }, this.state.config);

    return this.state.config;
  }

  /**
   * @public
   * @returns {Promise.<*>}
   */
  create() {
    if (this.state.creation) {
      return Promise.resolve(this.state.creation);
    }

    this.state.creation = Promise.resolve()
      .then(() => this.createQueues())
      .then(() => this.createExchanges())
      .then(() => this.createPolicies())
      .then(() => this.createUpstreams())
      .then(() => this.bindQueues())
      .then(() => this.createConsumers());

    return this.state.creation;
  }

  /**
   * @public
   * @returns {Promise.<*>}
   */
  createQueues() {
    return this.channel()
      .then(Channel => Promise.all(this.state.config.queues.reduce((acc, q) => {
        Object.keys(q.actions).forEach(action => {
          acc.concat([Channel.assertQueue(`${q.key}.${action}`, q.actions[action].options)]);
        });

        return acc;
      }, [Channel])));
  }

  /**
   * @public
   * @returns {Promise.<*>}
   */
  createExchanges() {
    return this.channel()
      .then(Channel => Promise.all(this.state.config.exchanges.reduce((acc, x) => {
        acc.concat([Channel.assertExchange(x.key, x.type, x.options)]);

        return acc;
      }, [Channel])));
  }

  /**
   * @public
   * @returns {Promise.<*>}
   */
  createConsumers() {
    return this.channel()
      .then(Channel => {
        this.state.config.queues.forEach(q => {
          Object.keys(q.actions).forEach(action => {
            const queue = q.actions[action];

            if (queue.consume && this.findExchange(queue.source)) {
              if (this.state.config.maxConcurrentJobs) Channel.prefetch(this.state.config.maxConcurrentJobs);
              Channel.consume(`${q.key}.${action}`, (msg) => queue.consume(msg, Channel));
            }
          });
        });
      });
  }

  /**
   * @public
   * @returns {Promise.<*>}
   */
  bindQueues() {
    return this.channel()
      .then(Channel => Promise.all(this.state.config.queues.reduce((acc, q) => {
          Object.keys(q.actions).forEach(action => {
            const queue = q.actions[action];

            const exchange = this.findExchange(queue.source);
            const queueAction = `${q.key}.${action}`;
            const routing = queue.pattern || queue.routingKey;

            if (exchange && !queue.noBind) {
              acc.concat([
                Channel.bindQueue(queueAction, exchange.key, routing),
              ]);
            }
          });

          return acc;
        }, [Channel]).concat(this.state.config.queuesSubscribe.reduce((acc, q) => {
          q.sources.forEach(source => {
            source.routingKeys.forEach(routingKey => {
              acc.push(
                Channel.assertExchange(source.exchange.name, source.exchange.type)
                  .then(() => {
                    return Channel.assertQueue(q.key);
                  })
                  .then((r) => {
                    return Channel.bindQueue(r.queue, source.exchange.name, routingKey);
                  })
              );
            });
          });

          return acc;
        }, []))
      ));
  }

  /**
   * @public
   * @returns {Promise.<*>}
   */
  channel() {
    if (this.state.channel) {
      return this.state.channel;
    }

    this.state.channel = this.connection().then(con => {
      return con.createChannel();
    });

    return this.state.channel;
  }

  /**
   * @returns {Promise.<*>}
   */
  connection() {
    if (this.state.connection) {
      return this.state.connection;
    }

    this.state.connection = amqp.connect(this.state.config.url)
      .then(con => {
        console.log('[rabbit-exchange] Connected :)');
        return con;
      });

    return this.state.connection;
  }

  /**
   * @public
   * @returns {Promise.<*>}
   */
  createPolicies() {
    const createPolicy = (policy) => {
      policy.vhost = policy.vhost || '/';

      const options = {
        method: 'PUT',
        path: `/api/policies/${encodeURIComponent(policy.vhost)}/${encodeURIComponent(policy.name)}`,
        auth: `${this.state.config.user}:${this.state.config.pass}`,
        hostname: `${this.state.config.host}`,
        port: this.state.config.adminPort,
        headers: {
          'Content-Type': 'application/json',
        },
      };

      return new Promise((resolve, reject) => {
        const req = http.request(options, res => resolve(res));
        req.on('error', err => reject(err));
        req.write(JSON.stringify(policy));
        req.end();
      });
    };

    const promises = this.state.config.policies.reduce((acc, p) => {
      acc.concat([createPolicy(p)]);
      return acc;
    }, []);

    return Promise.all(promises);
  }

  /**
   * @public
   * @returns {Promise.<*>}
   */
  createUpstreams() {
    const createUpstream = (upstream) => {
      upstream.vhost = upstream.vhost || '/';
      const path = `${encodeURIComponent(upstream.vhost)}/${encodeURIComponent(upstream.name)}`;

      const options = {
        method: 'PUT',
        path: `/api/parameters/federation-upstream/${path}`,
        auth: `${this.state.config.user}:${this.state.config.pass}`,
        hostname: `${this.state.config.host}`,
        port: this.state.config.adminPort,
        headers: {
          'Content-Type': 'application/json',
        },
      };

      return new Promise((resolve, reject) => {
        const req = http.request(options, res => resolve(res));
        req.on('error', err => reject(err));
        req.write(JSON.stringify(upstream));
        req.end();
      });
    };

    return Promise.all(this.state.config.upstreams.reduce((acc, u) => {
      return acc.concat([createUpstream(u)]);
    }, []));
  }

  /**
   * @public
   * @returns {*}
   */
  findExchange(name) {
    return this.state.config.exchanges.find(obj => obj.name === name);
  }

  /**
   * @public
   * @returns {Promise.<*>}
   */
  close() {
    if (!this.state.connection) {
      return Promise.resolve();
    }

    return this.state.channel.then((ch) => {
      if (ch) ch.close();
    }).then(() => {
      this.state.channel = null;
      return this.state.connection;
    }).then((con) => {
      if (con) con.close();
    }).then(() => {
      this.state.connection = null;
    }).catch(() => {
      this.state.channel = null;
      this.state.connection = null;
    });
  }

  /**
   * @public
   * @returns {Helper}
   */
  clear() {
    this.state.channel = null;
    this.state.connection = null;
    this.state.creation = null;
    return this;
  }
}

module.exports = {
  Helper,
};
