const { Helper } = require('./Helper');

describe('Helper', () => {
  it('should return a constructor', () => expect(typeof Helper).toBe('function'));
});
