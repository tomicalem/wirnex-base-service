const { RMQ } = require('./RMQ');

process.on('unhandledRejection', (reason, promise) => {
  console.log(reason, promise);
  return Promise.resolve();
});

describe('RMQ-Exchange', () => {
  it('should pass', () => expect(true).toBe(true));
  it('should return a singleton', () => expect(typeof RMQ).toBe('function'));

  it('should create a channel', () => {
    const RMQX = new RMQ();
    return RMQX.channel()
      .then(Channel => {
        expect(RMQX.state.connection).toBeDefined();
        expect(RMQX.state.channel).toBeDefined();
        expect(Channel.connection).toBeDefined();
        expect(Channel.ch).toBe(1);
      })
  });

  it('should publish a message to a the exchange', () => {
    const RMQX = new RMQ();
    return RMQX.publishTo('Campaigns', 'create', { 'msg': 'Hello World' })
      .then(results => {
        expect(results).toBe(true);
      });
  });

  it('should publish a message to the queue', () => {
    const RMQX = new RMQ();
    return RMQX.queue('Campaigns', 'create', { 'msg': 'Hello World' })
      .then(results => expect(results).toBe(true));
  });

  it('should consume a message in the queue', (done) => {
    const RMQX = new RMQ();
    RMQX.queue('Campaigns', 'create', { 'msg': 'Hello World' })
      .then(() => RMQX.consumeFrom('Campaigns', 'create', (msg, ch) => {
        expect(msg.content.toString()).toBe(JSON.stringify({ msg: 'Hello World' }));
        ch.ack(msg);
      }));

    done();
  });

  it('should create and close a channel', () => {
    const RMQX = new RMQ();
    return RMQX.channel()
      .then(() => RMQX.close())
      .then(() => {
        expect(RMQX.state.channel).toBe(null);
      });
  });
});
