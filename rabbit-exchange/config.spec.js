const config = require('./config');

describe('config', () => {
  it('should return a constructor', () => expect(typeof config).toBe('object'));
});
