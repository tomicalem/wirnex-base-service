const index = require('./index');

describe('index', () => {
  it('should return a singleton', () => expect(typeof index).toBe('object'));
});
