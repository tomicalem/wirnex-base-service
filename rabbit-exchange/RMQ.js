const { Helper } = require('./Helper');

class RMQ {
  /**
   * @public
   */
  constructor() {
    this.consumers = [];
    this.consumersSubscribe = [];
    this.state = {
      connection: null,
      channel: null,
      creation: null,
      config: {},
    };
    this.helper = new Helper(this.state);
    this.helper.generateConfiguration();
    this.initialize();
  }

  /**
   * @protected
   * @returns {Promise<* | never>}
   */
  initialize() {
    return Promise.resolve()
      .then(() => this.helper.clear())
      .then(() => this.helper.create())
      .then(() => this.helper.connection())
      .then(connection => connection.on("close", () => this.reinitialize()))
      .catch((err) => {
        console.log(`[rabbit-exchange] error: ${err}`);
        this.reinitialize();
      });
  }

  /**
   * @protected
   * @return {RMQ}
   */
  reinitialize() {
    console.log("[rabbit-exchange] reconnecting");
    setTimeout(() => {
      this.initialize().then(() => this.rehydrate());
    }, 1000);
    return this;
  }

  /**
   * @returns {RMQ}
   */
  rehydrate() {
    if (this.consumers && this.consumers.length) {
      this.consumers.forEach((consumer) => {
        this.consumeFromWaitQueueAsync(consumer.q, consumer.action, consumer.callback);
      });
    }
    if (this.consumersSubscribe && this.consumersSubscribe.length) {
      this.consumersSubscribe.forEach((consumer) => {
        this.consumeFromSubscribeAsync(consumer.q, consumer.callback);
      });
    }
    return this;
  }

  /**
   * @public
   * @param {string} q
   * @param {string} action
   * @param content
   * @param options
   * @returns {Promise<T | never>}
   */
  publishTo(q, action, content, options) {
    return this.helper.channel()
      .then(Channel => {
        const queue = this.state.config.queues.filter(arr => arr.name === q)[0].actions[action];
        const exchange = this.helper.findExchange(queue.source);
        const buffer = this.helper.contentToBuffer(content);
        const results = Channel.publish(exchange.key, queue.routingKey, buffer, options);
        if (!results) throw new Error(results);
        return results;
      });
  }

  /**
   * @public
   * Publish message to exchange
   * @param content {String} Message body
   * @param actionName {String} action from the actionsPublish section of the rabbit-config.js
   * @returns {Promise.<*>}
   */
  publishToExchange(content, actionName) {
    const action = this.state.config.actionsPublish[actionName];
    let _channel = null;
    return Promise.resolve()
      .then(() => this.helper.create())
      .then(() => this.helper.channel())
      .then((Channel) => {
        _channel = Channel;
        const exchange = this.state.config.exchanges.filter(arr => arr.name === action.exchange)[0];
        return _channel.assertExchange(exchange.key, exchange.type);
      })
      .then((a) => {
        return _channel.publish(a.exchange, action.routingKey, this.helper.contentToBuffer(content));
      });
  }

  /**
   * Wait for the queue to be created
   * @protected
   * @param q {String} Queue key
   * @param action {String} Queue action
   * @param opts {Object} An object that might carry options for channel.assertQueue
   * @returns {Promise}
   */
  assertQueue(q, action, opts = {}) {
    if (!q) return Promise.reject(`No queue ${q} with action ${action} found`);
    return Promise.resolve()
      .then(() => this.helper.create())
      .then(() => this.helper.channel())
      .then((ch) => {
        const queue = this.state.config.queues.filter(arr => arr.name === q)[0];
        return ch.assertQueue(`${queue.key}.${action}`, opts);
      });
  }

  /**
   * @public
   * @param q {String} Queue key
   * @param action {String} Queue action
   * @param callback {Function} An object that might carry options for channel.assertQueue
   * @returns {Promise.<*>}
   */
  consumeFromWaitQueue(q, action, callback) {
    this.consumers.push({ q, action, callback });
    return this.consumeFromWaitQueueAsync(q, action, callback);
  }

  /**
   * @protected
   * @param q {String} Queue key
   * @param action {String} Queue action
   * @param callback {Function} An object that might carry options for channel.assertQueue
   * @returns {Promise.<*>}
   */
  consumeFromWaitQueueAsync(q, action, callback) {
    return Promise.resolve()
      .then(() => this.helper.create())
      .then(() => this.assertQueue(q, action))
      .then(() => {
        return this.consumeFrom(q, action, (msg, _channel) => {
          callback(msg, _channel);
        });
      })
      .catch((err) => {
        console.error(`[rmq-exchange] failed setting up consumer ${q}-${action}.`, err.toString());
        return Promise.reject(err);
      });
  }

  /**
   * @protected
   * @param {string} q
   * @param {string} action
   * @param callback
   * @returns {Promise<*>}
   */
  consumeFrom(q, action, callback) {
    return Promise.resolve()
      .then(() => this.helper.create())
      .then(() => this.helper.channel())
      .then(channel => {
        const queue = this.state.config.queues.filter(arr => arr.name === q)[0];
        if (this.state.config.maxConcurrentJobs) {
          channel.prefetch(this.state.config.maxConcurrentJobs);
        }
        return channel.consume(`${queue.key}.${action}`, (msg) => callback(msg, channel));
      });
  }

  /**
   * @public
   * @param queueConfigName {String} from rabbit configuration file
   * @param callback function(msg, channel) {}
   * @return {*}
   */
  consumeFromSubscribe(queueConfigName, callback) {
    this.consumersSubscribe.push({ q: queueConfigName, callback });
    return this.consumeFromSubscribeAsync(queueConfigName, callback);
  }

  /**
   * @public
   * @param queueConfigName {String} from rabbit configuration file
   * @param callback function(msg, channel) {}
   * @return {Promise.<*>}
   */
  consumeFromSubscribeAsync(queueConfigName, callback) {
    let _channel = null;
    return Promise.resolve()
      .then(() => this.helper.create())
      .then(() => this.helper.channel())
      .then((Channel) => {
        _channel = Channel;
        const queue = this.state.config.queuesSubscribe
          .filter(arr => arr.name === queueConfigName)[0];
        return _channel.assertQueue(queue.key);
      })
      .then((r) => {
        if (this.state.config.maxConcurrentJobs) {
          _channel.prefetch(this.state.config.maxConcurrentJobs);
        }
        _channel.consume(`${r.queue}`, (msg) => {
          callback(msg, _channel);
        });
      })
      .catch((err) => {
        // console.log(`[rabbit-exchange] ${err}`);
      });
  }

  /**
   * @public
   * @param {string} q
   * @param {string} action
   * @param {*} content
   * @param {object} [options]
   * @returns {Promise<* | never>}
   */
  queue(q, action, content, options) {
    return Promise.resolve()
      .then(() => this.helper.create())
      .then(() => this.helper.channel())
      .then(Channel => {
        const queue = this.state.config.queues.filter(arr => arr.name = q)[0];
        return Channel.sendToQueue(`${queue.key}.${action}`, this.helper.contentToBuffer(content), options);
      });
  }

  /**
   * @public
   * @return {Promise<*>}
   */
  channel() {
    return this.helper.channel();
  }

  /**
   * @public
   * @return {Promise<*>}
   */
  close() {
    return this.helper.close();
  }

  /**
   * @public
   * @return {Object}
   */
  get config() {
    return this.state.config;
  }
}

module.exports = {
  RMQ,
};
