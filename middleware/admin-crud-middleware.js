const { MiddlewareBase } = require('./middleware-base');

 /**
 * @namespace
 * @property {CrudData} data
 */
class AdminCrudMiddleware extends MiddlewareBase {
  /**
   * @private
   * @param {Request} req
   * @return {object}
   */
  getUserRestrictions(req) {
    return {};
  }
}

module.exports = {
  AdminCrudMiddleware,
};
