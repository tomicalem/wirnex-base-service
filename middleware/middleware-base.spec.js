const { MiddlewareBase } = require('./middleware-base');

describe('index', () => {
  /** @type {MiddlewareBase} */
  let m;
  beforeEach(() => {
    m = Promise.promisifyAll(new MiddlewareBase());
  });

  describe('class methods', () => {
    it('should error on getUserRestrictions', () => {
      expect(m.getUserRestrictions).toThrowError('getUserRestrictions is not implemented');
    });
  });
});
