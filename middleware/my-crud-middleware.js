const { MiddlewareBase } = require('./middleware-base');

/**
 * @todo: extends MiddlewareBase
 * @namespace
 * @property {CrudData} data
 */
class MyCrudMiddleware extends MiddlewareBase {

  getUserRestrictions(req) {
    return { project_id: req.projectId };
  }
}

module.exports = {
  MyCrudMiddleware,
};
