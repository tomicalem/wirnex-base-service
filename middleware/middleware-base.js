const _ = require('lodash');
const restifyErrors = require('restify-errors');

const { Pagination } = require('../pagination');
const log = require('../log');

class MiddlewareBase {
  /**
   * @param {string} name
   * @param {object} data
   */
  constructor(name, data) {
    this.name = name;
    this.data = data;
    this.errors = restifyErrors;
    this.log = log.module(`middleware/${this.name}`);
  }

  /**
   * @public
   * @param {Request} req
   * @param {object} req.query
   * @param {Response} res
   * @param {Next} next
   */
  getAll(req, res, next) {
    const restrictions = this.getUserRestrictions(req);
    const pagination = this.getRequestPagination(req);
    const filter = this.getRequestFilter(req.query, pagination);
    this.log.trace(`${this.name} getAll`, JSON.stringify({ restrictions, pagination, filter }));
    this.dataToMiddleware(() => this.data.getAll(filter, pagination, restrictions), res, next);
  }

  /**
   * @public
   * @param {Request} req
   * @param {object} req.params
   * @param {string} req.params.id
   * @param {object} req.query
   * @param {Response} res
   * @param {Next} next
   */
  getOne(req, res, next) {
    const params = req.params;
    const filter = req.query;
    const restrictions = this.getUserRestrictions(req);
    this.log.trace(`${this.name} getOne`, { params, filter, restrictions });
    this.dataToMiddleware(() => this.data.getOne(params, filter, restrictions), res, next);
  }

  /**
   * @public
   * @param {Request} req
   * @param {object} req.body
   * @param {Response} res
   * @param {Next} next
   */
  createOne(req, res, next) {
    const data = req.body;
    const restrictions = this.getUserRestrictions(req);
    this.log.trace(`${this.name} createOne`, { data, restrictions });
    this.dataToMiddleware(() => this.data.createOne(data, restrictions), res, next);
  }

  /**
   * @public
   * @param {Request} req
   * @param {object} req.params
   * @param {string} req.params.id
   * @param {object} req.body
   * @param {Response} res
   * @param {Next} next
   */
  updateOne(req, res, next) {
    const params = req.params;
    const data = req.body;
    const restrictions = this.getUserRestrictions(req);
    this.log.trace(`${this.name} updateOne`, { params, data, restrictions });
    this.dataToMiddleware(() => this.data.updateOne(params, data, restrictions), res, next);
  }

  /**
   * @public
   * @param {Request} req
   * @param {object} req.params
   * @param {string} req.params.id
   * @param {Response} res
   * @param {Next} next
   */
  deleteOne(req, res, next) {
    const id = req.params.id;
    const restrictions = this.getUserRestrictions(req);
    this.log.trace(`${this.name} deleteOne`, { id });
    this.dataToMiddleware(() => this.data.deleteOne(id, restrictions), res, next);
  }

  /**
   * Aggregate
   * @param {Request} req
   * @param {Response} res
   * @param {Next} next
   */
  aggregate(req, res, next) {
    const promiseFn = () => this.data.aggregate(req.query);
    this.dataToMiddleware(promiseFn, res, next);
  }

  /**
   * @protected
   * @param {Function<Promise>} promiseFn
   * @param {Response} res
   * @param {Next} next
   * @returns {Promise<void>}
   */
  dataToMiddleware(promiseFn, res, next) {
    return Promise.resolve()
      .then(() => promiseFn())
      .then(/** @param {{ data: Object, pagination?: Object }} data */(data) => {
        res.send(data);
        next();
      })
      .catch((e) => {
        if (_.get(e, 'statusCode') === 500) {
          this.log.error('Server Error', e);
        }
        next(this.formatError(e));
      });
  }

  /**
   * @protected
   * @param {*} e
   * @returns {*}
   */
  formatError(e) {
    if (!e) {
      return new this.errors.InternalServerError('Unknown Error');
    }
    if (e) {
      if (e.statusCode) {
        return e;
      }
      return new this.errors.InternalServerError(`${e.message}`);
    }
  }

  /**
   * @protected
   * @param {Request} req
   * @returns {{project_id: string}|{user_id: string}|*}
   */
  getUserRestrictions(req) {
    throw new Error('getUserRestrictions is not implemented');
  }

  /**
   * @protected
   * @param {Request} req
   * @returns {PaginationBase}
   */
  getRequestPagination(req) {
    return Pagination.make(req.query);
  }

  /**
   * @protected
   * @param {*} params
   * @param {PaginationBase} pagination
   */
  getRequestFilter(params, pagination) {
    const nonFilterParams = [
      ...pagination.paramList,
    ];
    return _.omit(params, nonFilterParams);
  }
}

module.exports = { MiddlewareBase };
