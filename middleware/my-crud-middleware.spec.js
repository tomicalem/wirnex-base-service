jest.mock('../data/crud-data');
jest.mock('../pagination/pagination');

const { MyCrudMiddleware } = Promise.promisifyAll(require('./my-crud-middleware'));
const { CrudData } = require('../data/crud-data');

describe('index', () => {
  /** @type {MyCrudMiddleware} */
  let m;
  const pagination = { limit: 2, offset: 3 };
  beforeEach(() => {
    m = new MyCrudMiddleware('the name', new CrudData());
  });

  describe('call data methods', () => {
    test('should call getMyAll', async () => {
      const res = { send: jest.fn() };
      await m.getAllAsync({ query: { q: 1, a: 1 }, projectId: '656' }, res);
      expect(m.data.getAll).toBeCalledWith({ q: 1 }, pagination, { project_id: '656'});
      expect(res.send).toHaveBeenCalled();
    });

    test('should call getMyOne', async () => {
      const res = { send: jest.fn() };
      await m.getOneAsync({ query: { q: 2 }, params: { p: 2 }, projectId: '456' }, res);
      expect(m.data.getOne).toBeCalledWith({ p: 2 }, { q: 2 }, { project_id: '456'})
    });

    test('should call createMyOne', async () => {
      const res = { send: jest.fn() };
      await m.createOneAsync({ query: { q: 3 }, params: { p: 3 }, body: { b: 3 }, projectId: '789' }, res);
      expect(m.data.createOne).toBeCalledWith({ b: 3 }, { project_id: '789'})
    });

    test('should call updateMyOne', async () => {
      const res = { send: jest.fn() };
      await m.updateOneAsync({ query: { q: 4 }, params: { p: 4 }, body: { b: 4 }, projectId: '012' }, res);
      expect(m.data.updateOne).toBeCalledWith({ p: 4 }, { b: 4 }, { project_id: '012'})
    });

    test('should call deleteMyOne', async () => {
      const res = { send: jest.fn() };
      await m.deleteOneAsync({ query: { q: 5 }, params: { p: 5, id: 55 }, projectId: '345' }, res);
      expect(m.data.deleteOne).toBeCalledWith(55, { project_id: '345'})
    });
  });

  describe('handle errors', () => {
    test('should format errors', () => {
      m.data.getOne.mockImplementationOnce(() => Promise.reject(new Error('some error')));
      expect(m.getOneAsync({ params: {} }, {})).rejects.toThrowError('some error');
    });

    test('should format errors', () => {
      m.data.getOne.mockImplementationOnce(() => Promise.reject());
      expect(m.getOneAsync({ params: {} }, {})).rejects.toThrowError('Unknown Error');
    });
  });
});
