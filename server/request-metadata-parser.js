const errors = require('restify-errors');

function requestMetadataParser() {
  return (req, res, next) => {
    // user
    const userJson = req.headers['x-user'];
    if (userJson && typeof userJson === 'string') {
      try {
        req.user = JSON.parse(userJson);
      } catch (e) {
        return next(new errors.BadRequestError('Could not parse request user'));
      }
    }

    if (req.user) {
      // user roles
      const userRolesString = req.headers['x-user-roles'];
      if (userRolesString && typeof userRolesString === 'string') {
        const userRoles = `${userRolesString}`.split(',').map(r => r.trim().toLowerCase()).filter(r => r.length > 0);
        if (userRoles.length === 0) {
          return next(new errors.BadRequestError('Invalid list of user roles'));
        }
        req.userRoles = userRoles;
      } else {
        req.userRoles = ['guest'];
      }

      // project ID
      const requestProjectId = req.headers['x-project-id'];
      if (requestProjectId && typeof requestProjectId === 'string' && /^[0-9a-f]{24}$/.test(requestProjectId)) {
        req.projectId = requestProjectId;
      }
    } else {
      req.userRoles = ['guest'];
    }
    next();
  };
}

module.exports = {
  requestMetadataParser,
};
