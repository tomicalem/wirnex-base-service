const restify = require('restify');

const config = require('../config/server');

const { requestMetadataParser } = require('./request-metadata-parser');
const { defaultRoutes } = require('../routes/default-routes');
const { initErrorReporting } = require('../error-reporting');

const serverConfig = {
  name: config.name,
  version: config.version,
};

let server = null;

function createServer() {
  if (server) return server;

  server = restify.createServer(serverConfig);

  server
    .on('error', onError)
    .on('listening', onListening)
    .on('listening', defaultRoutes(server))
    .use(restify.plugins.bodyParser({ mapParams: false }))
    .use(restify.plugins.queryParser({ mapParams: false, arrayLimit: 10000 }))
    .use(restify.plugins.gzipResponse())
    .listen(config.port);

  if (!config.disableRequestMetadataParser) {
    server.use(requestMetadataParser());
  }

  // Disable Error Reporting
  //initErrorReporting(server);
  return server;
}

function onError(err) {
  console.log(err);
  throw new Error(err);
}

function onListening() {
  console.info(`Listening on port ${config.port}`);
}

module.exports = createServer;
