/**
 * @type {{ dsn: (*|string), options: * }}
 */
module.exports = {
  dsn: process.env.MONGO_DSN || `mongodb://${process.env.MONGO_HOST}:27017/${process.env.MONGO_DB}`,
  options: {
    autoReconnect: true,
    keepAlive: true,
    socketTimeoutMS: 0,
    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000,
    useNewUrlParser: true,
  },
};
