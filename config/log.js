const app = require('../wbs-config');

module.exports = {
  app: {
    name: app.name || 'app',
    version: app.version || '1.0.0',
  },
  /*remote_auth: {
    keyFilename: process.env.LOG_STACKDRIVER_AUTH || '/secrets/stackdriver/gcloud-auth.json',
  },*/
  local_level: process.env.LOG_LEVEL_LOCAL,
  remote_level: process.env.LOG_LEVEL_REMOTE,
};
