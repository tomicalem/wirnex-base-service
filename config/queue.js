const app = require('../wbs-config');

module.exports = {
  maxConcurrentJobs: 15,
  entityName: `${app.name}`.toLowerCase().replace(/^observer-/, '').replace(/-api$/, ''),
};
