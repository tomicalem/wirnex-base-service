const app = require('../wbs-config');
const env = require('./env');

/** @type {RmqExchangeConfig} */
module.exports = {
  rmqExchange: {

    maxConcurrentJobs: 15,

    queuesSubscribe: [{
      name: app.entityNamePluralUcfirst,
      key: env.isProduction ? app.entityNamePlural : `test${app.entityNamePluralUcfirst}`,
      sources: [],
    }],

    actionsPublish: {
      create: { exchange: 'Direct', routingKey: `${app.entityNamePlural}.create` },
      update: { exchange: 'Direct', routingKey: `${app.entityNamePlural}.update` },
      destroy: { exchange: 'Direct', routingKey: `${app.entityNamePlural}.destroy` },
    },

    exchanges: [
      { name: 'Direct', key: 'amq.direct', type: 'direct' },
    ],

    policies: [
      {
        name: env.isProduction ? `${app.entityNamePluralUcfirst} Federation` : `Test ${app.entityNamePluralUcfirst} Federation`,
        pattern: env.isProduction ? `^${app.entityNamePlural}` : `^test${app.entityNamePluralUcfirst}`,
        definition: {'federation-upstream-set': 'all'},
        priority: 0,
        'apply-to': 'all',
      },
    ],

    upstreams: [],
  },
};
