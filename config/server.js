const app = require('../wbs-config');

module.exports = {
  port: process.env.PORT || 80,
  name: app.name,
  version: app.version,
  disableRequestMetadataParser: process.env.DISABLE_REQUEST_METADATA_PARSER,
};
