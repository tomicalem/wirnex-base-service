const _ = require('lodash');

class Response {
  constructor(rawResponse) {
    this._rawResponse = rawResponse;
    this.data = this.getData();
    this.pagination = this.getPagination();
  }

  toJSON() {
    return {
      data: this.data,
      pagination: this.pagination,
    };
  }

  getRawresponse() {
    return this._rawResponse;
  }

  setRawresponse(rawResponse) {
    this._rawResponse = rawResponse;
    return this;
  }

  getData() {
    return _.get(this._rawResponse, 'data');
  }

  getPagination() {
    return _.get(this._rawResponse, 'pagination');
  }

  getLimit() {
    return _.get(this._rawResponse, 'pagination.limit');
  }

  getPage() {
    return _.get(this._rawResponse, 'pagination.page');
  }

  getPages() {
    return _.get(this._rawResponse, 'pagination.pages');
  }

  getTotal() {
    return _.get(this._rawResponse, 'pagination.total');
  }

  getSort() {
    return _.get(this._rawResponse, 'pagination.sort');
  }
}

module.exports = {
  Response,
};
