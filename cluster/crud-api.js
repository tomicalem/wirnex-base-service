const { ApiBase } = require('./api-base');

class CrudApi extends ApiBase {
  /**
   * @param filter
   * @param pagination
   * @returns {*}
   */
  getAll(filter = {}, pagination = {}) {
    return this.request({ qs: { ...filter, ...pagination } })
      .then(raw => this.makeList(raw));
  }

  /**
   * @param id
   * @returns {*}
   */
  getOne(id) {
    return this.request({ url: `${this.basePath}/${id}` })
      .then(raw => this.makeEntity(raw));
  }

  /**
   * @param data
   * @returns {*}
   */
  createOne(data) {
    return this.request({ method: 'POST', json: data })
      .then(raw => this.makeEntity(raw));
  }

  /**
   * @param id
   * @param data
   * @returns {*}
   */
  updateOne(id, data) {
    return this.request({ method: 'PUT', url: `${this.basePath}/${id}`, json: data })
      .then(raw => this.makeEntity(raw));
  }

  /**
   * @param id
   * @returns {*}
   */
  deleteOne(id) {
    return this.request({ method: 'DELETE', url: `${this.basePath}/${id}` })
      .then(raw => this.makeEntity(raw));
  }

  /**
   * @param {object} filter
   * @param {boolean} [required]
   * @returns {Promise<*>}
   */
  getFirst(filter, required = true) {
    return this
      .getAll(filter)
      .then(pagination => Array.isArray(pagination.data) ? pagination.data : [])
      .spread((theFirst) => {
        if (theFirst) {
          return theFirst;
        }
        // @todo: ... ? ... : null
        return required
          ? Promise.reject(Object.assign(new Error('List is empty'), { statusCode: 404 }))
          : undefined;
      });
  }
}

module.exports = { CrudApi };
