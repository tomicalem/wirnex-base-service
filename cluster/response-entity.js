const _ = require('lodash');

const { Response } = require('./response');

class ResponseEntity extends Response {
  getEntity() {
    return _.get(this._rawResponse, 'data', {});
  }
}

module.exports = {
  ResponseEntity,
};
