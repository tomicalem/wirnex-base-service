const _ = require('lodash');

const { Response } = require('./response');

class ResponseList extends Response {
  getList() {
    return _.get(this._rawResponse, 'data', []);
  }
}

module.exports = {
  ResponseList,
};
