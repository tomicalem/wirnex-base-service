const rp = require('request-promise');
const _ = require('lodash');

const app = require('../wbs-config');

const log = require('../log');

const { ResponseEntity } = require('./response-entity');
const { ResponseList } = require('./response-list');

class ApiBase {

  get identity() { return this._identity; }
  get protocol() { return this._protocol; }
  get serviceName() { return this._serviceName; }
  get version() { return this._version; }
  get hostname() { return this._hostname; }
  get basePath() { return this._basePath; }
  get serviceAccount() { return _.cloneDeep(this._serviceAccount); }
  get baseOptions() { return _.cloneDeep(this._baseOptions); }

  setIdentity(identity) { this._identity = identity; return this; }
  setProtocol(protocol) { this._protocol = protocol; return this; }
  setServiceName(serviceName) { this._serviceName = serviceName; return this; }
  setVersion(version) { this._version = version; return this; }
  setHostname(hostname) { this._hostname = hostname; return this; }
  setBasePath(basePath) { this._basePath = basePath; return this; }
  setServiceAccount(serviceAccount) { this._serviceAccount = serviceAccount; return this; }
  setBaseOptions(baseOptions) { this._baseOptions = baseOptions; return this; }

  /**
   * Constructor
   * @param {'http'|'https'} protocol
   * @param {string} serviceName
   * @param {string|null|undefined} version
   * @param {string} [basePath]
   */
  constructor(protocol, serviceName, version, basePath = '') {
    this._identity = `${app.name}`.toLowerCase();
    this._protocol = protocol;
    this._serviceName = `${serviceName}`.toLowerCase();
    this._version = version ? `${version}`.toLowerCase() : null;
    this._basePath = basePath;
    this._hostname = `${ this.serviceName }${ (this.version ? `-${this.version}` : '') }`;

    this._serviceAccount = {
      id: null,
      email: `${this.identity}@services.uptm.io`,
      first_name: this.identity,
      last_name: 'service',
      roles: ['admin'],
    };

    this._baseOptions = {
      method: 'GET',
      baseUrl: `${this.protocol}://${this.hostname}`,
      url: this.basePath !== '' ? basePath : '/',
      headers: {
        'x-user': JSON.stringify(this.serviceAccount),
        'x-user-roles': this.serviceAccount.roles.join(','),
      },
      json: true,
      gzip: true,
    };

    this.rp = rp;
    this.log = log.module(`cluster/${this.identity}/to/${this.hostname}`);

    this._options = { wrap: false };
  }

  setOptions(options) {
    this._options = options;
  }

  setOptionsWrap(value) {
    this.setOptions({...this._options, wrap: value });
  }

  getOptionsWrap() {
    return this._options.wrap;
  }

  /**
   * @public
   * @param {object} options
   * @returns {*}
   */
  request(options = {}) {
    const opts = this.baseOptions;
    // @todo: mmm...might it be like this:
    // @todo: Object.assign({}, options, this.baseOptions) ?
    _.merge(opts, options);
    return this.requestRaw(opts);
  }

  /**
   * @public
   * @param {object} options
   * @returns {Promise<*>}
   */
  requestRaw(options) {
    this.log.trace('request', JSON.stringify(options));
    return Promise
      .resolve(this.rp(options))
      .tapCatch(e => this.log.trace(`request error ${e}`));
  }

  /**
   * @param raw
   * @return {ResponseList}
   */
  makeList(raw) {
    if (this.getOptionsWrap() === true) {
      return new ResponseList(raw);
    }
    return raw;
  }

  /**
   * @param raw
   * @return {ResponseEntity}
   */
  makeEntity(raw) {
    if (this.getOptionsWrap() === true) {
      return new ResponseEntity(raw);
    }
    return raw;
  }
}

module.exports = { ApiBase };
