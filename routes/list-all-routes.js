/**
 * @param server
 * @return {{DELETE: Array, POST: Array, GET: Array, PUT: Array}}
 */
function listAllRoutes(server) {
  const map = {
    GET: [],
    POST: [],
    PUT: [],
    DELETE: [],
  };
  const routes = server.router.getRoutes();
  for (const key in routes) {
    const route = routes[key];
    map[route.method].push(route.path);
  }
  return map;
}

module.exports = { listAllRoutes };
