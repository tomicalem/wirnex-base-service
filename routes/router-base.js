const restifyErrors = require('restify-errors');
const Path = require('path');
const _ = require('lodash');

const log = require('../log');
const { RoutingAcl } = require('../acl/routing-acl');

class RouterBase {

  getServer() { return this.server; }
  getBasePath() { return this.basePath; }
  getAcl() { return this.acl; }
  getCrudMap() { return this.crudMap; }
  getDefaultCrudActions() { return this.defaultCrudActions; }

  setServer(server) { this.server = server; return this; }
  setBasePath(basePath) { this.basePath = basePath; return this; }
  setAcl(acl) { this.acl = acl; return this; }
  setCrudMap(crudMap) { this.crudMap = crudMap; return this; }
  setDefaultCrudActions(defaultCrudActions) { this.defaultCrudActions = defaultCrudActions; return this; }

  /**
   * @param {Object} server
   * @param {string} [basePath]
   * @param {object} [options]
   */
  constructor(
    server,
    basePath = '/',
    options = {},
  ) {
    if (typeof basePath !== 'string' || !/^\//.test(basePath)) {
      throw new Error('Invalid basePath');
    }

    this.server = server;
    this.basePath = basePath;
    this.log = log.module('router-base');
    this.errors = restifyErrors;

    this.acl = new RoutingAcl();

    this.crudMap = options.crudMap || {
      // getAll: { method: 'get', path: '/' },
      // getOne: { method: 'get', path: '/:id' },
      // createOne: { method: 'post', path: '/' },
      // updateOne: { method: 'put', path: '/:id' },
      // deleteOne: { method: 'del', path: '/:id' },
    };

    this.aclConfig = options.aclConfig || {
      // getAll: ['admin'],
      // getOne: ['admin'],
      // createOne: ['admin'],
      // updateOne: ['admin'],
      // deleteOne: ['admin'],
    };

    this.defaultCrudActions = options.defaultCrudActions || [
      // 'getAll',
      // 'getOne',
      // 'createOne',
      // 'updateOne',
      // 'deleteOne',
    ];
  }

  /**
   * @private
   * @param {string} url
   * @param middleware
   */
  get(url, ...middleware) {
    this.server.get(this.buildUrl(url), ...middleware);
  }

  /**
   * @private
   * @param {string} url
   * @param middleware
   */
  post(url, ...middleware) {
    this.server.post(this.buildUrl(url), ...middleware);
  }

  /**
   * @private
   * @param {string} url
   * @param middleware
   */
  put(url, ...middleware) {
    this.server.put(this.buildUrl(url), ...middleware);
  }

  /**
   * @private
   * @param {string} url
   * @param middleware
   */
  del(url, ...middleware) {
    this.server.del(this.buildUrl(url), ...middleware);
  }

  /**
   * @param {string} url
   * @returns {string}
   */
  buildUrl(url) {
    const p = Path.join(this.basePath, url).replace(/\\/g, '/');
    if (p.length > 1) {
      return p.replace(/\/$/, '');
    }
    return p;
  }

  /**
   * CRUD Mapper
   * @param {CrudMiddleware} crudMiddleware
   * @param {{ actions?: string[], before?: Function[], after?: Function[] }} [options]
   */
  crud(
    crudMiddleware,
    options = {},
  ) {
    options = options || {};
    options.actions = options.actions || this.defaultCrudActions;
    options.before = options.before || [];
    options.after = options.after || [];

    for (let action of options.actions) {
      if (action in crudMiddleware && action in this.crudMap) {

        const crudMethod = this.crudMap[action].method.toLowerCase();
        const crudPath = this.crudMap[action].path;
        const aclConfig = this.aclConfig[action] || [];

        const map = Path.join('/', this.basePath, crudPath);

        this.log.trace(
          `Mapping: "${crudMethod} ${map}"` +
          ` -> ${_.get(crudMiddleware, 'constructor.name')}.${action}()` +
          ` : ${aclConfig.join(', ')}`
        );

        this[crudMethod](
          crudPath,
          ...options.before,
          this.checkAccessMiddleware(action),
          crudMiddleware[action].bind(crudMiddleware),
          ...options.after
        );
      }
    }
  }

  /**
   * Get an ACL middleware
   * @param {string} action Middleware action
   * @returns {function(*, *, *): Promise<boolean>}
   * @protected
   */
  checkAccessMiddleware(action) {
    return (req, res, next) => {
      const userRoles = req.userRoles;
      const requiredRoles = this.aclConfig[action];
      this.acl
        .checkAccess(userRoles, requiredRoles)
        .then(() => next())
        .catch(e => next(e));
    }
  }
}

module.exports = { RouterBase };
