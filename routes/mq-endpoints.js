const _ = require('lodash');
const config = require('rabbit-config');

function mqEndpoints() {
  const out = {
    subscribe: [],
    publish: [],
  };

  _.each(config.queuesSubscribe, (sub) => {
    _.each(sub.sources, (source) => {
      _.each(source.routingKeys, (key) => {
        out.subscribe.push(`name: ${sub.key}, exchange: ${source.exchange.name} (${source.exchange.type}), key: ${key}`);
      });
    });
  });

  _.each(config.actionsPublish, (pub) => {
    const exch = _.find(config.exchanges, { name: pub.exchange });
    out.publish.push(`exchange: ${exch.key} (${exch.type}), key: ${pub.routingKey}`);
  });

  return out;
}

module.exports = {
  mqEndpoints,
};
