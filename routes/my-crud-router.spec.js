const { MyCrudRouter } = require('./my-crud-router');

describe('index', () => {
  describe('prefixes in url', () => {
    test.each([
      [undefined, undefined, '', '/my'],
      [undefined, 'meow', '', '/meow'],
      [undefined, 'meow', 'oink', '/meow/oink'],
      ['resource-usage/my', '', 'calls-log', '/resource-usage/my/calls-log'],
      ['monitors', 'my', '/', '/my/monitors'],
      ['a', 'b', 'c', '/b/a/c'],
    ])('routerBase %s, prefix %s, endpoint %s, final %s', (routerBase, prefix, endpoint, final) => {
      const server = { get: jest.fn() };
      const router = new MyCrudRouter(server, routerBase, prefix);
      router.crudMap = {
        get: { method: 'get', path: endpoint },
      };
      router.crud({
        get: jest.fn(),
      }, { actions: ['get'] });
      expect(server.get).toHaveBeenCalledWith(final, expect.any(Function), expect.any(Function));
    });
  });

  describe('config', () => {
    test('crud map, acl config', () => {
      const router = new MyCrudRouter({});
      expect(router.crudMap).toMatchObject({
        getAll: { method: 'get', path: '/' },
        aggregate: { method: 'get', path: '/aggregate' },
        getOne: { method: 'get', path: '/:id' },
        createOne: { method: 'post', path: '/' },
        updateOne: { method: 'put', path: '/:id' },
        deleteOne: { method: 'del', path: '/:id' },
      });
      expect(router.aclConfig).toMatchObject({
        getAll: ['user', 'admin'],
        getOne: ['user', 'admin'],
        aggregate: ['user', 'admin'],
        createOne: ['user', 'admin'],
        updateOne: ['user', 'admin'],
        deleteOne: ['user', 'admin'],
      });
      expect(router.defaultCrudActions).toMatchObject([
        'getAll',
        'aggregate',
        'getOne',
        'createOne',
        'updateOne',
        'deleteOne',
      ]);
    });
  });
});
