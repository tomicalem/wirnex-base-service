function defaultRoutes(server) {
  return () => {
    server.get('/health', (req, res) => res.send({ status: 'OK' }));
  };
}

module.exports = {
  defaultRoutes,
};
