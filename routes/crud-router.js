const { RouterBase } = require('./router-base');

/**
 * @implements {acl.IACLable}
 */
class CrudRouter extends RouterBase {

  /**
   * @param {Object} server
   * @param {string} [basePath]
   * @param {object} [options]
   */
  constructor(
    server,
    basePath = '/',
    options = {},
  ) {
    super(server, basePath, options);

    this.crudMap = options.crudMap || {
      getAll: { method: 'get', path: '/' },
      getOne: { method: 'get', path: '/:id' },
      createOne: { method: 'post', path: '/' },
      updateOne: { method: 'put', path: '/:id' },
      deleteOne: { method: 'del', path: '/:id' },
      aggregate: { method: 'get', path: '/aggregate' },
    };

    this.aclConfig = options.aclConfig || {
      getAll: ['admin'],
      getOne: ['admin'],
      createOne: ['admin'],
      updateOne: ['admin'],
      deleteOne: ['admin'],
      aggregate: ['admin'],
    };

    this.defaultCrudActions = options.defaultCrudActions || [
      'getAll',
      'aggregate',
      'getOne',
      'createOne',
      'updateOne',
      'deleteOne',
    ];
  }
}

module.exports = { CrudRouter };
