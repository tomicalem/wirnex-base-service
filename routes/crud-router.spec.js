const { CrudRouter } = require('./crud-router');

describe('index', () => {
  describe('config', () => {
    test('crud map, acl config', () => {
      const router = new CrudRouter({});
      expect(router.crudMap).toMatchObject({
        getAll: { method: 'get', path: '/' },
        aggregate: {method: 'get', path: '/aggregate'},
        getOne: { method: 'get', path: '/:id' },
        createOne: { method: 'post', path: '/' },
        updateOne: { method: 'put', path: '/:id' },
        deleteOne: { method: 'del', path: '/:id' },
      });
      expect(router.aclConfig).toMatchObject({
        getAll: ['admin'],
        aggregate: ['admin'],
        getOne: ['admin'],
        createOne: ['admin'],
        updateOne: ['admin'],
        deleteOne: ['admin'],
      });
      expect(router.defaultCrudActions).toMatchObject([
        'getAll',
        'aggregate',
        'getOne',
        'createOne',
        'updateOne',
        'deleteOne',
      ]);
    });
  });
});
