const Path = require('path');

const { RouterBase } = require('./router-base');

/**
 * @implements {acl.IACLable}
 */
class MyCrudRouter extends RouterBase {

  /**
   * @param {Object} server
   * @param {string} [basePath]
   * @param {string} [myPrefix]
   * @param {object} [options]
   */
  constructor(
    server,
    basePath = '/',
    myPrefix = 'my',
    options = {},
  ) {
    super(server, Path.join('/', myPrefix, basePath), options);

    /** @type {CrudMap} */
    this.crudMap = options.crudMap || {
      getAll: { method: 'get', path: '/' },
      aggregate: { method:'get', path:'/aggregate' },
      getOne: { method: 'get', path: '/:id' },
      createOne: { method: 'post', path: '/' },
      updateOne: { method: 'put', path: '/:id' },
      deleteOne: { method: 'del', path: '/:id' },
    };

    /** @type {AclConfig} */
    this.aclConfig = options.aclConfig || {
      getAll: ['user', 'admin'],
      aggregate: ['user', 'admin'],
      getOne: ['user', 'admin'],
      createOne: ['user', 'admin'],
      updateOne: ['user', 'admin'],
      deleteOne: ['user', 'admin'],
    };

    /** @type {string[]} */
    this.defaultCrudActions = options.defaultCrudActions || [
      'getAll',
      'aggregate',
      'getOne',
      'createOne',
      'updateOne',
      'deleteOne',
    ];
  }
}

module.exports = { MyCrudRouter };
