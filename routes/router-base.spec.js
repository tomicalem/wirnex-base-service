const { RouterBase } = require('./router-base');

describe('index', () => {
  /** @type {RouterBase} */
  let r;
  let server;
  beforeEach(() => {
    const req = { userRoles: ['manager'] };
    server = {
      get: jest.fn(async (path, ...fn) => {
        fn.forEach(f => f(req, {}, () => {}));
        server._get = jest.fn(async (done) => {
          try {
            await Promise.all(fn.map(f => Promise.promisify(f)(req, {})));
            done();
          } catch (e) {
            done(e);
          }
        });
      }),
      post: jest.fn((path, ...fn) => {
        fn.forEach(f => f(req, {}, () => {}));
      }),
      put: jest.fn((path, ...fn) => {
        fn.forEach(f => f(req, {}, () => {}));
      }),
      del: jest.fn((path, ...fn) => {
        fn.forEach(f => f(req, {}, () => {}));
      }),
    };
    r = new RouterBase(server, '/a');
    jest.spyOn(r.acl, 'checkAccess').mockImplementation(() => Promise.resolve());
  });

  describe('setup server routes', () => {
    test('it should partly setup get and post routes', () => {
      const middleware = {
        getSomething: jest.fn(),
        postSomething: jest.fn(),
      };
      r.crudMap = {
        getSomething: { method: 'GET', path: '/get-something' },
        postSomething: { method: 'post', path: '/post-something' },
      };
      r.crud(middleware, { actions: ['getSomething', 'postSomething'] });
      expect(server.get).toHaveBeenCalledWith('/a/get-something', expect.any(Function), expect.any(Function));
      expect(server.post).toHaveBeenCalledWith('/a/post-something', expect.any(Function), expect.any(Function));
      expect(middleware.getSomething).toHaveBeenCalled();
      expect(middleware.postSomething).toHaveBeenCalled();
      expect(server.put).not.toHaveBeenCalled();
      expect(server.del).not.toHaveBeenCalled();
    });

    test('it should partly setup put and delete routes', () => {
      const middleware = {
        deleteSomething: jest.fn(),
        putSomething: jest.fn(),
      };
      r.crudMap = {
        deleteSomething: { method: 'del', path: '/delete-something' },
        putSomething: { method: 'put', path: '/put-something' },
      };
      r.crud(middleware, { actions: ['deleteSomething', 'putSomething'] });
      expect(server.del).toHaveBeenCalledWith('/a/delete-something', expect.any(Function), expect.any(Function));
      expect(server.put).toHaveBeenCalledWith('/a/put-something', expect.any(Function), expect.any(Function));
      expect(middleware.deleteSomething).toHaveBeenCalled();
      expect(middleware.putSomething).toHaveBeenCalled();
      expect(server.get).not.toHaveBeenCalled();
      expect(server.post).not.toHaveBeenCalled();
    });
  });

  describe('build urls', () => {
    test.each([
      [undefined, '', '/'],
      [undefined, '', '/'],
      ['/', '', '/'],
      ['/my', '', '/my'],
      ['/foo', 'bar', '/foo/bar'],
      ['/how', 'far/:is', '/how/far/:is'],
    ])('routerBase %s, endpoint %s, final %s', (routerBase, endpoint, final) => {
      const router = new RouterBase(server, routerBase);
      router.crudMap = {
        get: { method: 'get', path: endpoint },
      };
      router.crud({
        get: jest.fn(),
      }, { actions: ['get'] });
      expect(server.get).toHaveBeenCalledWith(final, expect.any(Function), expect.any(Function));
    });
  });

  describe('check access', () => {
    test('should setup check access middleware', async () => {
      const middleware = {
        getSomething: jest.fn((a, b, next) => next()),
      };
      r.crudMap = {
        getSomething: { method: 'GET', path: '/get-something' },
      };
      r.aclConfig['getSomething'] = ['admin'];
      r.crud(middleware, { actions: ['getSomething', 'postSomething'] });
      expect(r.acl.checkAccess).toBeCalledWith(['manager'], ['admin']);
      await Promise.promisify(server._get)();
    });

    test('throw an error', async () => {
      jest.spyOn(r.acl, 'checkAccess').mockImplementation(() => Promise.reject(new Error('something happened')));
      const middleware = {
        getSomething: jest.fn((a, b, next) => next()),
      };
      r.crudMap = {
        getSomething: { method: 'GET', path: '/get-something' },
      };
      r.crud(middleware, { actions: ['getSomething', 'postSomething'] });
      await expect(Promise.promisify(server._get)()).rejects.toThrow('something happened');
    });
  });

  describe('constructor', () => {
    test.each([
      [{}, 1],
      [{}, 'hello'],
      [{}, '']
    ])('should error on wrong constructor call', (a, b) => {
      expect.assertions(1);
      expect(() => new RouterBase(a, b)).toThrowError('Invalid basePath');
    });
  });
});
