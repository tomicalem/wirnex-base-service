const { PaginationBase } = require('./pagination-base');
const _ = require('lodash');

/**
 * @class PaginationStd
 * @property {number} page
 * @property {number} limit
 * @property {object} sort
 */
class PaginationStd extends PaginationBase {
  get paramList() {
    return ['page', 'limit', 'sort'];
  }

  /**
   * @param {number} [page]
   * @param {number} [limit]
   * @param {string} [sort] A string like '-id', or an object like { id: -1 }, or { id: 1 }, [or 'asc'|'desc' (compatibility)]
   */
  constructor(
    {
      page,
      limit,
      sort,
    } = {}
  ) {
    super();
    this.page = parseInt(page) || 1;
    if (this.page < 1) {
      this.page = 1;
    }
    this.limit = parseInt(limit) || 10;
    if (this.limit < 0) {
      this.limit = 10;
    }
    this.sort = this.parseSort(sort);
  }

  /**
   * @private
   * @param {*} rawSort
   * @returns {Object}
   */
  parseSort(rawSort) {
    const defaultSort = { _id: -1 };
    if (!rawSort) { return defaultSort; }

    const sortObject = (_.isString(rawSort) || _.isNumber(rawSort))
      ? this.parseSortString(rawSort)
      : (_.isPlainObject(rawSort) ? this.parseSortObject(rawSort) : null);

    return _.isEmpty(sortObject) ? defaultSort : sortObject;
  }

  /**
   * @private
   * @param {*} sortStr
   * @return {null|{_id: (1|-1)}|Object<1|-1>}
   */
  parseSortString(sortStr) {
    const safeStr = _.toString(sortStr).trim();

    // legacy format support ('asc'/'desc')
    // todo: migrate services
    const legacySort = this.parseSortDirection(safeStr);
    if (legacySort) { return { _id: legacySort }; }
    // /legacy format support

    const m = safeStr.match(/^(-)?(.+)$/i);
    if (m) {
      return this.parseSortObject({ [m[2]]: `${m[1]==='-' ? '-1' : '1'}` }); }
    return null;
  }

  /**
   * @private
   * @param {Object} sortObj
   * @return {Object}
   */
  parseSortObject(sortObj) {
    return _.chain(sortObj)
      .omit()
      .toPairs()
      .map(([key, val]) => [key, this.parseSortDirection(val)])
      .filter(([key, sortDirection]) => this.isColumnName(key) && sortDirection)
      .fromPairs()
      .value();
  }

  /**
   * @private
   * @param {*} direction
   * @return {1|-1|null}
   */
  parseSortDirection(direction) {
    const lcStr = _.toString(direction).trim().toLowerCase();
    return ['1', 'asc'].includes(lcStr)
      ? 1
      : (['-1', 'desc'].includes(lcStr) ? -1 : null);
  }

  /**
   * @private
   * @param {*} str
   * @return {boolean}
   */
  isColumnName(str) {
    return _.isString(str) && /^[a-z_]+[\w_]*$/i.test(str);
  }
}

module.exports = {
  PaginationStd,
};
