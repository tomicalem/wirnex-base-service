const { Pagination } = require('./pagination');
const { PaginationStd } = require('./pagination-std');
const { PaginationHp } = require('./pagination-hp');

describe('pagination/pagination', () => {
  describe('static make()', () => {
    test.each([
      [{}, new PaginationStd({})],
      [{ page: '1', limit: '2', sort: '-col' }, new PaginationStd({ page: 1, limit: 2, sort: '-col' })],
      [{ last_id: 'x', limit: '9' }, new PaginationHp({ last_id: 'x', limit: 9 })]
    ])('it should return a pagination instance: %p ~> %p', (input, output) => {
      expect(Pagination.make(input)).toEqual(output);
    });
  });

  describe('static isPagination()', () => {
    test.each([
      ...[null, undefined, {}, [], { page: 1, limit: 2, sort: '-aaa' }].map(v => [v, false]),
      ...[new PaginationStd({}), new PaginationHp({ last_id: 'x' })].map(p => [p, true]),
    ])('it should check is it a pagination or not: %p ~> %p', (p, isPagination) => {
      expect(Pagination.isPagination(p)).toBe(isPagination);
    });
  });
});
