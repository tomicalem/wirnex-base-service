const { toNumber } = require('lodash');

const { PaginationBase } = require('./pagination-base');

class PaginationHp extends PaginationBase {
  get paramList() {
    return ['last_id', 'limit'];
  }

  /**
   * @param {string} [last_id]
   * @param {number} [limit]
   */
  constructor(
    {
      last_id,
      limit,
    } = {}
  ) {
    super();
    this.last_id = last_id || null;
    this.limit = toNumber(limit) || 10;
  }

  static name() {
    return 'HP';
  }

  name() {
    return PaginationHp.name();
  }
}

module.exports = {
  PaginationHp,
};
