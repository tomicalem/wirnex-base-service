const { PaginationHp } = require('./pagination-hp');

describe('index', () => {
  it('A', () => {
    expect(PaginationHp).toBeInstanceOf(Function);
  });
  it('2', () => {
    expect(new PaginationHp()).toEqual({"last_id": null, "limit": 10});
  });
  it('3', () => {
    expect(new PaginationHp({last_id: 'a', limit: 11})).toEqual({"last_id": 'a', "limit": 11});
  });
});
