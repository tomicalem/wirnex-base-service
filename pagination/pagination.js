const { get, has, omit } = require('lodash');

const { PaginationStd } = require('./pagination-std');
const { PaginationHp } = require('./pagination-hp');
const { PaginationBase } = require('./pagination-base');

/**
 * A pagination factory
 */
class Pagination {
  /**
   * @private
   */
  constructor() {
  }

  /**
   * Get pagination instance by params
   * @public
   * @tested
   * @param {object} params
   */
  static make(params) {
    const paramsObj = omit(params);
    if (has(paramsObj, 'last_id')) {
      return Pagination.getSHighPerformanceDefForReqSync(paramsObj);
    }
    return Pagination.getStdDefForReqSync(paramsObj);
  }

  /**
   * Is pagination?
   * @public
   * @param {*} obj
   * @returns {boolean}
   */
  static isPagination(obj) {
    return !!(obj && (obj instanceof PaginationBase));
  }

  /**
   * @deprecated
   * @tested
   * @public
   * @param {object} [params]
   * @param {RequestPaginationThroughPages | RequestPaginationHighPerformance} [params.query]
   * @returns {PaginationStd | PaginationHp}
   */
  static getRequestPagination(params) {
    console.warn('Pagination.getRequestPagination() is deprecated, use make() instead');
    if (has(params, 'last_id')) {
      return Pagination.getSHighPerformanceDefForReqSync(params);
    }
    return Pagination.getStdDefForReqSync(params);
  }

  /**
   * @protected
   * @param {object} [params]
   * @param {RequestPaginationThroughPages} [params.query]
   * @returns {PaginationStd}
   */
  static getStdDefForReqSync(params) {
    const offset = get(params, 'offset'); // XOR page
    const page = get(params, 'page');
    const limit = get(params, 'limit');
    const sort = get(params, 'sort');

    return new PaginationStd({ offset, page, limit, sort });
  }

  /**
   * @protected
   * @param {object} [params]
   * @param {RequestPaginationHighPerformance} [params.query]
   * @returns {PaginationHp}
   */
  static getSHighPerformanceDefForReqSync(params) {
    const last_id = get(params, 'last_id');
    const limit = get(params, 'limit');

    return new PaginationHp({ last_id, limit });
  }
}

module.exports = {
  Pagination,
};
