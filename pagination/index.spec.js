const index = require('./index');
const { Pagination } = require('./pagination');
const { PaginationBase } = require('./pagination-base');
const { PaginationStd } = require('./pagination-std');
const { PaginationHp } = require('./pagination-hp');

describe('pagination/index', () => {
  it('should return an index of classes', () => {
    expect(index).toBeInstanceOf(Object);
    expect(index.Pagination).toBe(Pagination);
    expect(index.PaginationBase).toBe(PaginationBase);
    expect(index.PaginationStd).toBe(PaginationStd);
    expect(index.PaginationHp).toBe(PaginationHp);
  });
});
