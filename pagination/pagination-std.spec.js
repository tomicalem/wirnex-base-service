const { PaginationStd } = require('./pagination-std');
const { PaginationBase } = require('./pagination-base');
const _ = require('lodash');

describe('pagination/pagination-std', () => {
  describe('constructor()', () => {
    test('it should be instance of PaginationBase', () => {
      expect(new PaginationStd()).toBeInstanceOf(PaginationBase);
    });

    test('it should provide a list of pagination keys', () => {
      expect(new PaginationStd().paramList).toMatchObject(['page', 'limit', 'sort']);
    });

    describe('arg parser', () => {
      const defaults = () => ({ page: 1, limit: 10, sort: { _id: -1 } });

      describe('defaults', () => {
        test.each(
          [{}, { foo: 'bar' }].map(v => [v])
        )('it should create default pagination from empty params (%p)', (params) => {
          expect(new PaginationStd(params)).toMatchObject(defaults());
        });
      });

      describe('.page', () => {
        test.each(
          _.flatMap([
            [
              [undefined, 'undefined', null, 'null', NaN, 'NaN', '', '  ', ' abc', 0, '0', '-0', 1, '1', -1, '-1', -1e1,
                Infinity, 'Infinity', -Infinity, '-Infinity', 'a1', '-1e1', '1a', '1e1'],
              defaults().page,
            ],
            [
              [2, '2', '2.5', 2.5, '2.2.2', '2a'],
              2,
            ],
            [
              [1e2, 100, '100'],
              100,
            ],
          ], ([values, expected]) => values.map(v => [v, expected])),
        )('it should transform integer page=%p to %p', (input, output) => {
          expect(new PaginationStd({ page: input }).page).toBe(output);
        });
      });

      describe('.limit', () => {
        test.each(
          _.flatMap([
            [
              [undefined, 'undefined', null, 'null', NaN, 'NaN', '', '  ', ' abc', 0, '0', '-0', -1, '-1', -1e1,
                Infinity, 'Infinity', -Infinity, '-Infinity', 'a1', '-1e1'],
              defaults().limit,
            ],
            [
              [1, '1', '1a', '1e1'],
              1,
            ],
            [
              [2, '2', '2.5', 2.5, '2.2.2', '2a'],
              2,
            ],
            [
              [1e2, 100, '100'],
              100,
            ],
          ], ([values, expected]) => values.map(v => [v, expected])),
        )('it should transform integer limit=%p to %p', (input, output) => {
          expect(new PaginationStd({ limit: input }).limit).toBe(output);
        });
      });

      describe('.sort', () => {
        test.each([
          ...[
            null, [ 'null', { 'null': 1 }], undefined, [ 'undefined', { 'undefined':1 }], NaN,
            [ 'NaN', { 'NaN': 1 }], {}, [], '', '  ',
            'desc', 'DeSC', '-1', -1, { _id: -1 }, { _id: '-1' }, { foo: 'bar' }, { '1a': -1 },
            '-2', '-&', '-', '%',
          ].map(v => [v, defaults().sort]),
          ['asc', { _id: 1 }],
          ['aSC', { _id: 1 }],
          ['1', { _id: 1 }],
          [1, { _id: 1 }],
          [{ f1: -1, f2: 'asc' }, { f1: -1, f2: 1 }],
          [{ f1: 1, f2: -1 }, { f1: 1, f2: -1 }],
          [{ f1: 'Asc', f2: 'DESc' }, { f1: 1, f2: -1 }],
          [{ f1: 'Asc', f2: 'unknown' }, { f1: 1 }],
          ['-updated_at', { updated_at: -1 }]
        ])('it should transform \'sort\' value %p to %p', (input, output) => {
          expect(new PaginationStd({ sort: input }).sort).toMatchObject(output);
        });
      });
    });
  });
});
