const { pick } = require('lodash');

/**
 * @abstract
 */
class PaginationBase {
  /**
   * @abstract
   * @return {string[]}
   */
  get paramList() {
    return [];
  }

  /**
   * @returns {*}
   */
  serialize() {
    return pick(this, this.paramList);
  }
}

module.exports = { PaginationBase };
