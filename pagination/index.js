const { Pagination } = require('./pagination');
const { PaginationBase } = require('./pagination-base');
const { PaginationHp } = require('./pagination-hp');
const { PaginationStd } = require('./pagination-std');

module.exports = {
  Pagination, // factory
  PaginationBase,
  PaginationHp,
  PaginationStd,
};
