const errors = require('restify-errors');

const { AclBase } = require('./acl-base');

/**
 * @implements {acl.IACLProvider}
 */
class RoutingAcl extends AclBase {
  constructor() {
    super();
    this.errors = errors;
  }

  /**
   * Check access
   * @param {string[]} userRoles
   * @param {string[]} requiredRoles
   * @returns {Promise<boolean>}
   */
  checkAccess(userRoles, requiredRoles) {
    if (!Array.isArray(userRoles) || userRoles.length === 0) {
      return Promise.reject(new this.errors.BadRequestError('Invalid request user roles'));
    }
    if (!Array.isArray(requiredRoles) || requiredRoles.length === 0) {
      return Promise.reject(new this.errors.InternalServerError('Invalid configuration for route roles'));
    }
    return super
      .checkAccess(userRoles, requiredRoles)
      .catch(e => Promise.reject(new this.errors.InternalServerError(`Could not check access: ${e}`)))
      .then(hasAccess => hasAccess ? Promise.resolve(true) : Promise.reject(new this.errors.ForbiddenError('Forbidden for your roles')));
  }
}

module.exports = { RoutingAcl };
