import AclBase from './acl-base';

namespace acl {
  export interface IACLProvider {
    checkAccess<T>(subjects: string[], accessList: string[]): Promise<T>;
  }
  export interface IACLable {
    acl: AclBase;
    aclConfig: any;
  }
}
