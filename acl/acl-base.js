const _ = require('lodash');

/**
 * @implements {acl.IACLProvider}
 */
class AclBase {
  /**
   * Check access
   * @param {Array<string>} subjects
   * @param {Array<string>} accessList
   * @returns {Promise<boolean>}
   */
  checkAccess(subjects, accessList) {
    if (!Array.isArray(subjects)) {
      return Promise.reject(new Error('ACL: subjects is not an array'));
    }
    if (!Array.isArray(accessList)) {
      return Promise.reject(new Error('ACL: accessList is not an array'));
    }
    return Promise.resolve(_.intersection(subjects, accessList).length > 0);
  }
}

module.exports = { AclBase };
