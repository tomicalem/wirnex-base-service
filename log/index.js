const config = require('../config/log');
const local_level = config.local_level;

if (process.env.NODE_ENV === 'development') {
  config.local_level = 0;
  config.remote_level = null;
}

const logger = require('bunyan-buddy')(config);

if (process.env.NODE_ENV === 'development') {
  const bunyan = require('bunyan');
  const PrettyStream = require('bunyan-prettystream');
  const prettyStdOut = new PrettyStream();
  prettyStdOut.pipe(process.stdout);

  const map = {
    trace: 10,
    debug: 20,
    info: 30,
    warn: 40,
    error: 50,
  };

  logger.streams.unshift({
    level: map[local_level] || 20,
    raw: true,
    stream: prettyStdOut,
  });
  logger._level = map[local_level] || 20;
  const options = {};
  Object.assign(options, {
    name: config.app.name,
    streams: logger.streams,
  });

  logger.module = (moduleName) => {
    return bunyan.createLogger(Object.assign({}, options, {module: moduleName}));
  };
}

module.exports = logger;
