const _ = require('lodash');

module.exports = {
  parseBoolean,
  requiredEnv,
};

/**
 *
 * @param {*} val
 * @param {boolean} against
 * @returns {boolean}
 */
function parseBoolean(val) {
  if (_.isBoolean(val)) {
    return val;
  }
  if (!_.isString(val)) {
    return !!val;
  }
  switch (val.trim().toLowerCase()) {
    case '0':
    case '-0':
    case 'nan':
    case 'undefined':
    case 'null':
    case '':
    case 'false':
    case 'f':
    case 'off':
    case 'no':
    case 'n':
      return false;
    default:
      return true;
  }
}

/**
 *
 * @param {string[]} varList
 * @param {string} [requester]
 */
function requiredEnv(varList, requester = 'Unknown') {
  if (!Array.isArray(varList) || varList.length === 0) return;
  varList.forEach((k) => {
    if (!process.env.hasOwnProperty(k) || process.env[k] === undefined) {
      throw new Error(`Configuration test failed: env variable '${k}' is required but not defined, requester: ${requester}`);
    }
  });
}
