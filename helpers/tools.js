const _ = require('lodash');

module.exports = {
  /**
   * @returns {number}
   */
  timestamp: () => Math.floor(new Date().getTime() / 1e3),
  /**
   * @param minMs
   * @param maxMs
   * @returns {*}
   */
  randomDelay: (minMs, maxMs) => Promise.delay(_.random(minMs, maxMs)),
  /**
   * @param obj
   * @returns {string}
   */
  debug: (obj) => typeof obj === 'object' ? `(${Object.keys(obj).map(k => `${k}=${obj[k]}`).join(', ')})` : `${obj}`,
  /**
   * @param {boolean} condition
   * @param {Function.<T>} fn
   * @returns {Promise.<T>}
   * @template {T}
   */
  resolveIf: (condition, fn) => condition ? Promise.resolve().then(() => fn()) : Promise.resolve(),
};
