const utils = require('./utils');

describe('parseBoolean', () => {
  test.each([
    ['0', false],
    ['-0', false],
    ['nan', false],
    ['undefined', false],
    ['null', false],
    ['', false],
    ['false', false],
    ['f', false],
    ['Off', false],
    ['no', false],
    ['n', false],
    [0.001, true],
    [-5, true],
    [0, false],
    [1, true],
    [null, false],
    [undefined, false],
    ['1', true],
    ['y', true],
    ['yes', true],
    ['t', true],
    ['true', true],
    ['on', true],
    [true, true],
    [false, false],
  ])('should convert %s to boolean', (val, expected) => {
    expect(utils.parseBoolean(val)).toEqual(expected);
  });
});
