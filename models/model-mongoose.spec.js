const _ = require('lodash');

const { ModelMongoose } = require('./model-mongoose');
const { PaginationStd, PaginationHp, Pagination } = require('../pagination');
const { MongooseModelMock } = require('./__mocks__/mongoose-model-mock');
const { MongooseModelMock2 } = require('./__mocks__/mongoose-model-mock-2');
const mongoose = require('../database').mongoose;

describe('models/model-mongoose', () => {
  /** @type {ModelMongoose} */
  let modelMongoose;
  /** @type {ModelMongoose} */
  let modelMongoose2;
  beforeEach(() => {
    modelMongoose = new ModelMongoose(MongooseModelMock);
    modelMongoose2 = new ModelMongoose(MongooseModelMock2);
  });
  afterEach(() => {
    MongooseModelMock.mockReset();
  });

  describe('constructor()', () => {
    describe('public properties', () => {
      test('it should be a list of model columns', () => {
        expect(modelMongoose.modelColumns).toMatchObject(['a', 'b']);
      });
    });
  });

  describe('getAll()', () => {
    describe('pagination', () => {
      beforeEach(() => {
        // ensure that std and hp pagination queries both are using same filter builder
        jest.spyOn(modelMongoose, 'buildFilter').mockImplementation(() => ({ filter: 'mocked' }));
      });
      afterEach(() => {
        modelMongoose.buildFilter.mockRestore();
      });

      describe('std pagination', () => {
        test.each([
          [undefined],
          [{}],
          [{ foo: 'bar' }],
          [{ limit: 800 }],
          [new PaginationStd({ page: 2, limit: 5, sort: { a: 'asc', b: -1 } })],
        ])('it should find records using STD pagination %p', async (p) => {
          const pg = new PaginationStd(p).serialize();
          const result = await modelMongoose.getAll({ a: 'b' }, p, { b: 'c' });
          const mock = result.data[0];
          expect(mock.paginateCalls).toMatchObject([[{ filter: 'mocked' }, pg]]);
          expect(result.pagination).toMatchObject({ limit: 10, page: 1, pages: 1, total: 3, sort: pg.sort });
          expect(result.data).toHaveLength(3);
          expect(result.data.slice(1)).toMatchObject([{ _id: '222' }, { _id: '333' }]);
        });
      });

      describe('HP pagination', () => {
        test.each([
          { last_id: 'xx', limit: 80 },
          new PaginationHp({ last_id: 'xx', limit: 8 }),
        ].map(p => [p]))('it should find records using HP pagination %p', async (p) => {
          const result = await modelMongoose.getAll({ a: 'b' }, p, { b: 'c' });

          expect(result.data[0].findCalls).toMatchObject([[{ filter: 'mocked' }]]);
          expect(result.data[0].sortCalls).toMatchObject([[{ _id: -1 }]]);
          expect(result.data[0].limitCalls).toMatchObject([[new PaginationHp(p).limit]]);
          expect(result.data).toHaveLength(2);
          expect(result.data[1]).toMatchObject({ _id: '222' });
        });
      });
    });

    describe('filter builder', () => {
      describe('w/o restrictions', () => {
        test.each([
          [{}, {}],
          [{a: 2, b: 'bbb', foo: 'bar'}, { a: 2, b: 'bbb' }],
          [{a: 2, b: 'bbb', foo: 'bar'}, { a: 2, b: 'bbb' }],
          [
            {
              oR: [
                { a: 'foo', foo: 'a', b: ['EXIstS', true] },
                { a: ['in', [1, 'two', 3, 4]], b: ['GREATER_THAN_OR_EQUAL_to', 2] },
                { aNd: [
                    { a: 1, b: 2, c: 3 },
                    { a: 1, Or: [ { b: 8 } ] },
                  ],
                },
              ],
            },
            // -----
            {
              $or: [
                { a: 'foo', b: { $exists: true } },
                {
                  a: { $in: [1, 'two', 3, 4] },
                  b: { $gte: 2 },
                },
                {
                  $and: [
                    { a: 1, b: 2 },
                    {
                      $or: [{ b: 8 }],
                      a: 1,
                    },
                  ],
                },
              ],
            },
          ],
        ])('it should build filter %p ~> %p', async (query, filter) => {
          const result = await modelMongoose.getAll(query, {}, {});
          expect(result.data[0].paginateCalls).toEqual([[filter, expect.objectContaining({})]]);
        });
      });

      describe('w/ restrictions', () => {
        test.each([
          [{}, {}, {}],
          [{}, { a: 'aaa' }, { a: 'aaa' }],
          [
            { a: 5, b: 6 },
            { a: 'aaa' },
            { $and: [{ a: 5, b: 6}, { a: 'aaa' } ]},
          ],
          [
            { Or: [{ a: 5 }, { b: 6 }] },
            { a: 'aaa' },
            { $and: [{ $or: [{ a: 5 }, { b: 6 }] }, { a: 'aaa' } ]},
          ],
        ])('it should build filter w/ top-level restrictions %p + %p ~> %p', async (query, restrictions, filter) => {
          const result = await modelMongoose.getAll(query, {}, restrictions);
          expect(result.data[0].paginateCalls).toEqual([[filter, expect.objectContaining({})]]);
        });
      });

      describe('mongoose types casting', () => {
        let save;
        beforeAll(() => {
          save = MongooseModelMock2.schema.paths;
          MongooseModelMock2.schema.paths = { created_at: {}, _id: {} };
          const map = {
            created_at: 'Date', _id: 'ObjectID',
          };
          MongooseModelMock2.schema.path.mockImplementation((p) => ({ instance: map[p] }));
        });

        afterAll(() => {
          MongooseModelMock2.schema.paths = save;
        });

        beforeEach(() => {
          jest.spyOn(mongoose.Types, 'ObjectId').mockImplementation((id) => new String(id));
          modelMongoose2.Model.exec.mockResolvedValue([{ _id: null, total: 15, results: [] }]);
        });

        test.each([
          [
            { or: [
              { created_at: ['>=', '2019-01-01T00:00:00Z'] },
              { id: ['IN', ['id0', 'id1']] },
            ] },
            { },
            [{
              $match: {
                $or: [
                  { created_at: { $gte: new Date('2019-01-01T00:00:00Z') } },
                  { _id: { $in: ['id0', 'id1'] } },
                ],
              },
            },
            { $group: { } },
            { $sort: { _id: -1 } },
            { $group: { _id: null, total: { $sum: 1 }, results: { $push: '$$ROOT' } } },
            { $project: { total: 1, results: { $slice: [ '$results', 0, 10 ] } } }],
            ['id0', 'id1'],
          ],
        ])('it should build filter w/ top-level restrictions %p + %p ~> %p', async (query, restrictions, expected, objectIdCalls) => {
          const pagination = Pagination.make(query);
          const result = await modelMongoose2.aggregate(query, pagination, restrictions);
          objectIdCalls.forEach(id => expect(mongoose.Types.ObjectId).toHaveBeenCalledWith(id));
          expect(result.data).toEqual([]);
          expect(result.pagination).toMatchObject({limit: 10, page: 1, pages: 2, total: 15, sort: pagination.sort });
          expect(modelMongoose2.Model.aggregate).toHaveBeenCalledWith(expected);
        });
      });

      describe('check on EXISTS type', () => {
        let save;
        beforeAll(() => {
          save = MongooseModelMock2.schema.paths;
          MongooseModelMock2.schema.paths = { a: {}, b: {} };
          const map = {
            a: 'Number', b: 'ObjectID',
          };
          MongooseModelMock2.schema.path.mockImplementation((p) => ({ instance: map[p] }));
        });

        test('should parse numbers according to schema, boolean in $exists', async () => {
          const pagination = Pagination.make();
          const query = { or: [
            { a: ['gt', '17'] },
            { b: ['EXISTS', 'no'] },
            { a: ['in', [10, '11']] },
          ] };
          await modelMongoose2.aggregate(query, pagination);
          expect(modelMongoose2.Model.aggregate).toHaveBeenCalledWith([
            expect.objectContaining({ '$match': {
              '$or': [{ a: { '$gt': 17 } }, { b: { '$exists': false } }, { a: { '$in': [10, 11] } }]
            } }),
            expect.objectContaining({ }),
            expect.objectContaining({ }),
            expect.objectContaining({ }),
            expect.objectContaining({ }),
          ]);
        });
      });
    });

    describe('errors', () => {
      beforeEach(() => {
        MongooseModelMock.mockExecImplementation(() => Promise.reject(new Error('Achtung!')));
      });

      test('it should return an execution error (std pagination)', async () => {
        const error = await modelMongoose.getAll({}, new PaginationStd(), {}).catch(e => e);
        expect(error.message).toBe('Achtung!');
      });

      test('it should return an execution error (hp pagination)', async () => {
        const error = await modelMongoose.getAll({}, new PaginationHp({ last_id: 'das letzte' }), {}).catch(e => e);
        expect(error.message).toBe('Achtung!');
      });
    });

    describe('aggregate', () => {
      beforeEach(() => {
        modelMongoose2.Model.exec.mockResolvedValue(undefined);
      });

      test('it should return empty array if returns undefined', async () => {
        const pagination = Pagination.make({});
        const results = await modelMongoose2.aggregate({}, pagination, {});
        expect(results).toEqual({ data: [], pagination: { limit: 10, page: 1, pages: 1, sort: { _id: -1 }, total: 0 } });
      });
    });
  });
});
