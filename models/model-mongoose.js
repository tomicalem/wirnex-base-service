const _ = require('lodash');
const Joi = require('joi');
const { Model } = require('./model');
const { Pagination, PaginationStd } = require('../pagination');
const mongoose = require('../database').mongoose;
const { parseBoolean } = require('../helpers/utils');

class ModelMongoose extends Model {
  constructor(model, options = null) {
    super(model);
    this.options = options || {};
  }
  /**
   * @public
   * @return {string[]}
   */
  get modelColumns() { return Object.keys(this.Model.schema.paths); }

  /**
   * @protected
   * @return {string[]}
   */
  get modelReadonlyColumns() { return ['_id', 'id', 'created_at', 'updated_at', 'deleted_at', '_version']; }

  /**
   * @protected
   * @returns {{name: string, aliases: string[]}[]}
   */
  get operatorMap() {
    return [
      { name: '$eq', aliases: ['=', 'EQUAL_TO'] },
      { name: '$gt', aliases: ['>', 'GREATER_THAN', 'GT'] },
      { name: '$gte', aliases: ['>=', 'GREATER_THAN_OR_EQUAL_TO', 'GTE'] },
      { name: '$in', aliases: ['IN'] },
      { name: '$lt', aliases: ['<', 'LESS_THAN', 'LT'] },
      { name: '$lte', aliases: ['<=', 'LESS_THAN_OR_EQUAL_TO', 'LTE'] },
      { name: '$ne', aliases: ['!=', 'NOT_EQUAL_TO'] },
      { name: '$nin', aliases: ['NOT_IN', 'NIN'] },
      { name: '$exists', aliases: ['EXISTS'] },
      { name: '$and', aliases: ['AND'], logical: true },
      { name: '$or', aliases: ['OR'], logical: true },
    ];
  }

  /**
   * Get all
   * @public
   * @param {Object<*>} filter
   * @param {PaginationBase} pagination
   * @param {Object<*>} [restrictions]
   * @return {Promise<{ data: Mongoose.Model[], pagination: * }>}
   */
  getAll(filter = {}, pagination = {}, restrictions = {}) {
    if (!Pagination.isPagination(pagination)) {
      pagination = Pagination.make(pagination);
    }

    return (pagination instanceof PaginationStd)
      ? this.getAllWithStdPagination(filter, restrictions, pagination)
      : this.getAllWithLastId(filter, restrictions, pagination);
  }

  /**
   * @protected
   * @param filterParams
   * @param restrictions
   * @param pagination
   * @returns {Promise<{pagination: {limit: number, last_id: string}, data: Mongoose.Model[]}>}
   */
  getAllWithLastId(filterParams, restrictions, pagination) {
    const filterFull = pagination.last_id
      ? { and: [
          { _id: { $lt: pagination.last_id } },
          filterParams,
        ] }
      : filterParams;

    const query = this.buildFilter(filterFull, restrictions);

    return this.Model
      .find(query)
      .sort({ _id: -1 })
      .limit(pagination.limit)
      .exec()
      .then(data => ({
        data: data,
        pagination: {
          last_id: data.length === 0 ? pagination.last_id : data[data.length - 1].id,
          limit: pagination.limit
        },
      }));
  }

  /**
   * @protected
   * @param {Object} filterParams
   * @param {Object} restrictions
   * @param {PaginationStd} pagination
   * @returns {Promise<{pagination: PaginationStd|{total: number, pages: number}, data: Mongoose.Model[]}>}
   */
  getAllWithStdPagination(filterParams, restrictions, pagination) {
    // @todo: offset it not used at the moment
    const query = this.buildFilter(filterParams, restrictions);
    return this.Model
      .paginate(
        query,
        pagination.serialize(),
      )
      .then(data => ({
        data: data.docs,
        pagination: {
          limit: data.limit,
          page: data.page,
          pages: data.pages,
          total: data.total,
          sort: pagination.sort,
        },
      }));
  }

  /**
   * Get one record
   * @param {string} id
   * @param {*} [filter]
   * @param {*} [restrictions]
   * @return {Promise<{data: void | Query | TSchema} | never | never>}
   */
  getOne(id, filter = {}, restrictions = {}) {
    return this
    .validateObjectId(id)
    .then(() => this.Model.findOne({ _id: id, ...filter, ...restrictions }))
    .then(model => model || Promise.reject(new this.errors.NotFoundError(`${this.name} not found`)))
    .then(data => ({ data }))
    .catch(e => this.handleDbError(e));
  }

  /**
   * Create one record
   * @param {*} data
   * @param {*} restrictions
   * @return {Promise<{data: any} | never | never>}
   */
  createOne(data, restrictions = {}) {
    const payload = _.omit(_.pick(data, this.modelColumns), this.modelReadonlyColumns);
    const modelData = payload;
    Object.assign(modelData, restrictions);
    return Promise.resolve()
      .tap(() => {
        const validate = this.options.validate;
        if (validate) {
          return Joi.validate(data, validate, { abortEarly: false });
        }
      })
      .then(() => new this.Model(modelData).save())
      .then(data => ({ data }))
      .catch(e => this.handleDbError(e));
  }

  /**
   * Update one record
   * @param {string} id
   * @param {*} data
   * @param {*} [restrictions]
   * @return {Promise<{data: void | Query | TSchema} | never | never>}
   */
  updateOne(id, data, restrictions = {}) {
    const modelData = _.omit(_.pick(data, this.modelColumns), [...this.modelReadonlyColumns, 'project_id', 'user_id']);
    return this
    .validateObjectId(id)
    .then(() => this.Model.findOne({ _id: id, ...restrictions }))
    .then(model => model || Promise.reject(new this.errors.NotFoundError(`${this.name} not found`)))
    .tap((model) => {
      const validate = this.options.validate;
      if (validate) {
        return Joi.validate({ ..._.omit(model.toObject(), this.modelReadonlyColumns), ...data }, validate, { abortEarly: false });
      }
    })
    .then((model => this.Model.update({ _id: model.id }, modelData, { 'new': true, runValidators: true })))
    .then(model => model || Promise.reject(new this.errors.NotFoundError(`${this.name} not found`)))
    .then(() => this.Model.findOne({ _id: id, ...restrictions }))
    .then(data => ({ data }))
    .catch(e => this.handleDbError(e));
  }

  /**
   * Delete one record
   * @param {string} id
   * @param {*} [restrictions]
   * @return {*}
   */
  deleteOne(id, restrictions = {}) {
    return this
      .validateObjectId(id)
      .then(() => this.Model.findOne({ _id: id, ...restrictions }))
      .then(model => model || Promise.reject(new this.errors.NotFoundError(`${this.name} not found`)))
      // TODO: return object here
      .tap((model => this.Model.deleteOne({ _id: model.id })))
      // .tap(deleted => this.sendModelEvent('deleted', deleted)) // FIXME - use data layer
      .then(() => ({ data: { success: true } })) // fixme: return model here
      .catch(e => this.handleDbError(e));
  }

  aggregate(filter, pagination, restrictions) {
    const match = this.buildFilter(filter, restrictions);
    // group
    const group = {};
    const groupSrc = _.get(filter, 'group', {});
    Object.keys(groupSrc).forEach(key => {
      _.set(group, key, /^\d+$/.test(groupSrc[key]) ? parseInt(groupSrc[key]) : groupSrc[key]);
    });
    const query = [{ '$group': group }];
    if (!_.isEmpty(match)) {
      query.unshift({ '$match': match });
    }
    const skip = (pagination.page - 1) * pagination.limit || 0;
    query.push(...[
      { $sort: pagination.sort },
      { $group: { _id: null, total: { $sum: 1 }, results: { $push: '$$ROOT' } } },
      { $project: { total: 1, results: { $slice: [ '$results', skip, pagination.limit ] } } }
    ]);

    return Promise.resolve()
      .then(() => this.Model.aggregate(query).exec())
      .then(data => _.get(data, '0', []))
      .then((data) => {
        const totalCount = _.get(data, 'total', 0);
        const pages = { pages: totalCount > pagination.limit ? Math.ceil(totalCount/pagination.limit) : 1};
        return {
          data: _.get(data, 'results', []),
          pagination: {...pagination, ...pages, total: totalCount}
        }
      });
  }

  /**
   * Build a hierarchical filter
   * @protected
   * @param {*} rawFilter
   * @param {*} restrictions
   * @return {*}
   */
  buildFilter(rawFilter = {}, restrictions = {}) {
    const primaryQuery = this.buildFilterNode(_.omit(rawFilter));
    const restrictionsQuery = this.buildFilterNode(_.omit(restrictions));

    return (_.isEmpty(primaryQuery) || _.isEmpty(restrictionsQuery))
      ? { ...primaryQuery, ...restrictionsQuery }
      : { $and: [primaryQuery, restrictionsQuery] };
  }

  /**
   * Build a filter node (logical expression or nested column condition)
   * @private
   * @param {*} node
   * @param {object|function} opFilter A filter for allowed operators
   * @param {'ObjectID' | 'Date' | null} typeFunction
   * @return {*}
   */
  buildFilterNode(node, opFilter = {}, typeFunction = null) {
    const allowedOperators = _.filter(this.operatorMap, opFilter);

    return _.chain(node)
      .omit()
      .toPairs()
      .map(([k, v]) => {
        // model column
        if (k === 'id') {
          k = '_id';
        }
        if (this.modelColumns.includes(k)) {
          const _typeFunction = _.get(this.Model.schema.path(k), 'instance');
          if (Array.isArray(v)) { // [op, [args...]]
            const nodeFromArr = this.transformFilterArrayToNode(v);
            return [k, this.buildFilterNode(nodeFromArr, {}, _typeFunction)];
          }
          return [k, this.buildFilterLeaf(v, _typeFunction)];
        }
        const opAlias = `${k}`.toUpperCase();
        const op = allowedOperators.find(o => o.aliases.includes(opAlias));
        if (!op) {
          return null;
        }

        if (op.name === '$exists') {
          return [op.name, this.buildFilterLeaf(v, 'Boolean')];
        }

        // logical operator
        if (op.logical) {
          if (!Array.isArray(v) || v.length === 0) {
            return null;
          }

          const opArgs = _.chain(v)
            .map(arg => this.buildFilterNode(arg, {}, typeFunction))
            .filter(q => !_.isEmpty(q))
            .value();
          if (opArgs.length === 0) {
            return null;
          }
          return [op.name, opArgs];
        }

        // non-logical operator (we're in a column-level query)
        return [op.name, this.buildFilterLeaf(v, typeFunction)];
      })
      .filter()
      .fromPairs()
      .value();
  }

  /**
   * Build a leaf (column-level conditions)
   * Example: rawValue | { <operator>: <arg>, ... }
   * @private
   * @param {*} leaf
   * @return {any[]|*}
   */
  buildFilterLeaf(leaf, typeFunction = null) {
    if (Array.isArray(leaf)) {
      return _.map(leaf, v => this.buildFilterLeaf(v, typeFunction));
    }
    if (!_.isPlainObject(leaf)) {
      if (typeFunction) {
        switch (typeFunction) {
          case 'Date':
            return new Date(leaf);
          case 'ObjectID':
            return new mongoose.Types.ObjectId(leaf);
          case 'Number':
            return _.toNumber(leaf);
          case 'Boolean':
            return parseBoolean(leaf);
        }
      }
      return leaf;
    }
    // conditions contain a nested object, - don't allow logical operators inside it
    return this.buildFilterNode(leaf, op => !op.logical, typeFunction);
  }

  /**
   * Transform array-like query to a node
   * Example: [IN, [1, 2]] -> { IN: [1, 2] }
   * @private
   * @param {[string, *]} arr
   * @return {*}
   */
  transformFilterArrayToNode(arr) {
    return _.fromPairs([arr]);
  }

  /**
   * Handle DB error
   *
   * - Server errors: statusCode = 500
   *
   * - Duplicate key errors: statusCode = 400, message = DUPLICATE_KEY, duplicateKeyInfo = { collection, index, key }
   * (check that 'key' contains some column name to find the reason)
   *
   * - Mongoose ValidationError: statusCode = 400, message = <original message>
   *
   * @protected
   * @param {Error|{ statusCode?: number }} error
   * @returns {Promise<never>} Returns a restify-compatible error
   */
  handleDbError(error) {

    this.log.debug('DB Error', error);

    const unknownError = new this.errors.InternalServerError('Unknown Error');
    const databaseError = new this.errors.InternalServerError('Database Error');

    if (!error) {
      return Promise.reject(unknownError);
    }

    // don't handle errors containing statusCode
    if (error.statusCode) {
      return Promise.reject(error);
    }

    // MongoDB errors
    // see https://github.com/mongodb/mongo/blob/master/src/mongo/base/error_codes.err

    if (error.name === 'MongoError') {

      const mongoCode = parseInt(error.code);
      if (!mongoCode) {
        return Promise.reject(databaseError);
      }

      // Input data errors
      if ([3, 84, 11000].includes(mongoCode)) {
        const dupMatches = `${error.message}`.match(/collection:\s+(.+)\s+index:(.+)\s+key:\s+(.+)$/);
        if (dupMatches) {
          const [dup_, dupCollection, dupIndex, dupKey] = dupMatches;
          const dupKeyProperty = this.modelColumns.find(c => `${dupIndex}`.includes(c)) || dupIndex;
          return Promise.reject(new this.errors.BadRequestError(`${this.name} with this ${dupKeyProperty} already exist`));
        }
        return Promise.reject(new this.errors.BadRequestError(`${this.name} already exist`));
      }

      // server-side errors
      return Promise.reject(databaseError);
    }

    // mongoose validation errors
    if (error.name === 'ValidationError') {
      return Promise.reject(new this.errors.BadRequestError(error.message));
    }

    return Promise.reject(new this.errors.InternalServerError(`Unknown Error: ${error}`));
  }

  /**
   * Validate Object ID
   * @private
   * @param {ObjectId} id
   * @return {Promise<void>}
   */
  validateObjectId(id) {
    return /^[0-9a-f]{24}$/.test(`${id}`) ?
      Promise.resolve() : Promise.reject(new this.errors.BadRequestError('Invalid ID'));
  }
}

module.exports = { ModelMongoose };
