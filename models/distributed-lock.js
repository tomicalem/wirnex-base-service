const mongoose = require('../database').mongoose;
const { ModelMongoose } = require('./model-mongoose');

const schema = mongoose.Schema({
  document_id: {
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    index: { unique: true }
  },
  client_id: { required: false, type: String },
}, {
  timestamps: { createdAt: 'created_at', updatedAt: false },
  toObject: { virtuals: true },
  toJSON: {
    virtuals: true,
    transform: (doc, ret) => {
      delete ret._id;
    },
  },
  versionKey: false,
});

schema.virtual('id').get(function () {
  return this._id;
});
schema.index({ created_at: 1 }, { expireAfterSeconds: 30 });

const DistributedLock = new ModelMongoose(mongoose.model('DistributedLock', schema));

module.exports = { DistributedLock };
