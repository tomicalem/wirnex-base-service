const errors = require('restify-errors');
const log = require('../log');

/**
 * @class Model
 * @abstract
 * @property {*} Model
 */
class Model {
  /**
   * @public
   * @returns {string}
   */
  get name() {
    return this.constructor.name;
  }

  /**
   * @public
   * @abstract
   * @return {string[]}
   */
  get modelColumns() {
    return [];
  }

  /**
   * @param {*} model
   */
  constructor(model) {
    if (this.constructor.name === 'Model') {
      throw new Error('Cannot create an instance of abstract class Model');
    }
    if (!model) {
      throw new Error('A model is required');
    }
    /**
     * @protected
     * @type {*}
     */
    this.Model = model;
    /**
     * @protected
     */
    this.errors = errors;

    /**
     * @protected
     */
    this.log = log.module(`models/${this.name}`);
  }

  /**
   * @public
   * @param {*} filter
   * @param {PaginationBase|*} [pagination]
   * @param {*} [restrictions]
   * @return {Promise<*[]>}
   */
  getAll(filter, pagination = {}, restrictions = {}) {
    return Promise.reject(new Error('getAll is not implemented.'));
  }

  /**
   * TODO: remove id, use filter
   * @public
   * @param {string} id
   * @param {*} [filter]
   * @param {*} [restrictions]
   * @returns {Promise<*>}
   */
  getOne(id, filter = {}, restrictions = {}) {
    return Promise.reject(new Error('getOne is not implemented.'));
  }

  /**
   * @public
   * @param {*} data
   * @param {*} [restrictions]
   * @returns {Promise<*>}
   */
  createOne(data, restrictions = {}) {
    return Promise.reject(new Error('createOne is not implemented.'));
  }

  /**
   * @public
   * @param {string} id
   * @param {*} data
   * @param {*} [restrictions]
   * @return {Promise<never>}
   */
  updateOne(id, data, restrictions = {}) {
    return Promise.reject(new Error('updateOne is not implemented.'));
  }

  /**
   * @public
   * @param {string} id
   * @param {*} [restrictions]
   * @return {Promise<*>}
   */
  deleteOne(id, restrictions = {}) {
    return Promise.reject(new Error('deleteOne is not implemented.'));
  }
}

module.exports = { Model };
