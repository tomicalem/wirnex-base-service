const { ErrorReporting } = require('@google-cloud/error-reporting');
const app = require('../wbs-config');

function initErrorReporting(server) {
  try {
    const keyFilename = process.env.LOG_STACKDRIVER_AUTH || '/secrets/stackdriver/gcloud-auth.json';
    const errors = new ErrorReporting({
      projectId: process.env.GCLOUD_PROJECT,
      keyFilename,
      // Specifies when errors are reported to the Error Reporting Console.
      // See the "When Errors Are Reported" section for more information.
      // Defaults to 'production'
      reportMode: 'production',
      // Determines the logging level internal to the library; levels range 0-5
      // where 0 indicates no logs should be reported and 5 indicates all logs
      // should be reported.
      // Defaults to 2 (warnings)
      logLevel: 2,
      serviceContext: {
        service: app.name || 'setup_APP_NAME_env_var',
        version: app.version || 'setup_APP_VERSION_env_var',
      }
    });
    if (server) {
      server.use(errors.restify(server));
    }
    process.on('uncaughtException', (e) => {
      errors.report(e);
    });
  } catch (e) {
    console.log(e);
  }
}

module.exports = { initErrorReporting };
