const _ = require('lodash');
const rp = require('request-promise');

const log = require('../log');

class GitBase {
  get baseOptions() {
    return _.cloneDeep(this._baseOptions);
  }

  /**
   * @param {string} name
   * @param {string} baseUrl
   */
  constructor(
    name,
    baseUrl,
  ) {
    this.baseUrl = baseUrl;
    this.name = name;
    this._baseOptions = {
      headers: {
        'User-Agent': 'uptm',
        'Content-Type': 'application/json',
      },
      json: true,
    };
    this.rp = rp;
    this.log = log.module(this.name);
  }

  /**
   * @param {string} method
   * @param {object} options
   * @returns {Promise<*>}
   */
  request(
    method,
    options = {},
  ) {
    options.method = method;
    const opts = this.baseOptions;
    _.merge(opts, options);

    return this.requestRaw(opts);
  }

  /**
   * @param {object} options
   * @returns {Promise<*>}
   */
  requestRaw(options) {
    return Promise
      .resolve(this.rp(options))
      .tapCatch(e => this.log.trace('request error', e));
  }
}

module.exports = { GitBase };
