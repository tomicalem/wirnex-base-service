const retry = require('bluebird-retry');

const { GitBase } = require('./git-base');

class GitHub extends GitBase {
  constructor() {
    super('github', 'https://api.github.com');
  }

  /**
   * @param {string} access_token
   * @returns {Promise<*>}
   */
  getRepositories(access_token) {
    let options = {
      url: `${this.baseUrl}/user/repos`,
      qs: { access_token: access_token, per_page: 100 }
    };

    return this.request('GET', options);
  }

  /**
   * @param build ALL Information about build
   * @param build.repo {string} repository URL
   * @param build.hash {string} git revision hash commit
   * @param build.access_token {string} access token for repository
   * @param build.status {string} build status on GitHub
   * @param build.frontendUrlLog {string} URL from UI
   * @param build.descriptionForStatus {string} description for status on GitHub
   * @param build.context {string} context - description about app
   * @returns {Promise<*>}
   */
  updateCommitStatus(build) {
    let options = {
      url: `${this.baseUrl}/repos/${build.repo.replace(/^\/+|\/+$/g, '')}/statuses/${build.hash}`,
      qs: { access_token: build.access_token },
      body: {
        state: build.status,
        target_url: `${build.frontendUrlLog}`,
        description: `${build.descriptionForStatus}`,
        context: `${build.context}`
      }
    };

    return retry(
      this.request,
      {
        context: this,
        args: ['POST', options],
      }
    );
  }
}

module.exports = GitHub;
