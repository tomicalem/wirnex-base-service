const log = require('../log').module('database');

/**
 * @abstract
 */
class DbBase {
  constructor() {
    this.log = log;
  }

  /**
   * @abstract
   */
  getConnection() {
    throw new Error('Not Implemented');
  }
}

module.exports = { DbBase };
