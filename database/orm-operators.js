/**
 * @deprecated
 */
module.exports.mongooseOperators = mongooseOperators = {
  '=': '$equals',
  'EQUAL_TO': '$equals',

  '>': '$gt',
  'GREATER_THAN': '$gt',

  '>=': '$gte',
  'GREATER_THAN_OR_EQUAL_TO': '$gte',

  'IN': '$in',

  '<': '$lt',
  'LESS_THAN': '$lt',

  '<=': '$lte',
  'LESS_THAN_OR_EQUAL_TO': '$lte',

  '!=': '$ne',
  'NOT_EQUAL_TO': '$ne',

  'NOT_IN': '$nin',
};
