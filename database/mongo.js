const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');
const deepPopulate = require('mongoose-deep-populate')(mongoose);
const {
  findByIdOrThrow,
  findOneAndUpdateWithHook,
  hasntOrThrow,
  hasOrThrow,
  replacements,
} = require('./plugins');
const _ = require('lodash');
const bluebird = require('bluebird');

const { DbBase } = require('./db-base');
const config = require('../config/mongo');

class Mongo extends DbBase {
  get config() {
    return _.cloneDeep(this._config);
  }

  constructor() {
    super();

    this.mongoose = mongoose;
    this.mongoose.Promise = bluebird;

    this.mongoose.plugin(mongoosePaginate);
    this.mongoose.plugin(deepPopulate);
    this.mongoose.plugin(findByIdOrThrow);
    this.mongoose.plugin(findOneAndUpdateWithHook);
    this.mongoose.plugin(hasntOrThrow);
    this.mongoose.plugin(hasOrThrow);
    this.mongoose.plugin(replacements);

    this._config = config;
  }

  /**
   * @returns {{mongoose: module:mongoose, db: Connection}}
   */
  getConnection() {
    this.mongoose
      .connect(this.config.dsn, this.config.options)
      .catch(err => this.log.error('Mongo: error', err));

    const db = this.mongoose.connection;

    db.on('error', (e) => this.log.error('Mongo: connection error', e));
    db.on('open', () => this.log.info('Mongo: connected!'));
    db.on('reconnected', () => this.log.info('Mongo: reconnected!'));
    db.on('disconnected', () => this.log.info('Mongo: disconnected!'));
    db.on('connecting', () => this.log.info('Mongo: connecting ...'));
    db.on('reconnectFailed', () => this.log.info('Mongo: gave up reconnecting'));

    return {
      db,
      mongoose: this.mongoose,
    };
  }
}

module.exports = { Mongo };
