const { hasntOrThrow: main } = require('./hasntOrThrow');

describe('main module tests', () => {
    describe('main', () => {
        it('A', () => {
            expect(main).toBeInstanceOf(Function);
        });
    });
});
