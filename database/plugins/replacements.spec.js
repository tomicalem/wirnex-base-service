const { main } = require('./replacements');

describe('main module tests', () => {
    describe('main', () => {
        it('A', () => {
            expect(main).toBeInstanceOf(Function);
        });
    });
});
