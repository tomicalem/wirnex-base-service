const { EntityExistsError } = require('./errors/EntityExistsError');

function plugin(...args) {
    return this.findOne(...args).then((result) => {
        if (result) {
            return Promise.reject(new EntityExistsError('An entity is exists.'));
        }
        return result;
    });
}

/**
 *
 * @param {object} schema
 * @todo write a test for me
 */
function hasntOrThrow(schema) {
    /* eslint-disable no-param-reassign */
    schema.statics.hasntOrThrow = plugin;
}

exports.plugin = plugin;
exports.hasntOrThrow = hasntOrThrow;
