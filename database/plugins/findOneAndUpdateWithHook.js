/**
 * @param {object} conditions
 * @param {object} createData
 * @param {object} updateDate
 * @returns {*|PromiseLike<T | never>|Promise<T | never>|t|b}
 */
function plugin(
    conditions,
    createData,
    updateDate,
) {
    return this.findOne(conditions).then((result) => {
        if (result) {
            return result.update(updateDate).then(() => this.findOne(conditions));
        }
        return new this(createData).save();
    });
}

/**
 * @param {object} schema
 * @todo write a test for me
 */
function findOneAndUpdateWithHook(schema) {
    /* eslint-disable no-param-reassign */
    schema.statics.findOneAndUpdateWithHook = plugin;
}

exports.plugin = plugin;
exports.findOneAndUpdateWithHook = findOneAndUpdateWithHook;
