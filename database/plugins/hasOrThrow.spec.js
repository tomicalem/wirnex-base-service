const { hasOrThrow: main } = require('./hasOrThrow');

describe('main module tests', () => {
    describe('main', () => {
        it('A', () => {
            expect(main).toBeInstanceOf(Function);
        });
    });
});
