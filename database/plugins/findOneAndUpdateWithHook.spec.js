const { findOneAndUpdateWithHook: main } = require('./findOneAndUpdateWithHook');

describe('main module tests', () => {
    describe('main', () => {
        it('A', () => {
            expect(main).toBeInstanceOf(Function);
        });
    });
});
