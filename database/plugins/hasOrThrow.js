const { NoEntityError } = require('./errors/NoEntityError');

function plugin(...arg) {
    return this.findOne(...arg)
        .then(result => result || Promise.reject(new NoEntityError('An entity is not found.')));
}

/**
 * @param {object} schema
 * @todo write a test for me
 */
function hasOrThrow(schema) {
    /* eslint-disable no-param-reassign */
    schema.statics.hasOrThrow = plugin;
}

exports.plugin = plugin;
exports.hasOrThrow = hasOrThrow;
