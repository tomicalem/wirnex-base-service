const { findByIdOrThrow } = require('./findByIdOrThrow');
const { findOneAndUpdateWithHook } = require('./findOneAndUpdateWithHook');
const { hasntOrThrow } = require('./hasntOrThrow');
const { hasOrThrow } = require('./hasOrThrow');
const { replacements } = require('./replacements');
const errors = require('./errors');

module.exports = {
    findByIdOrThrow,
    findOneAndUpdateWithHook,
    hasntOrThrow,
    hasOrThrow,
    replacements,
    ...errors,
};
