const { NoEntityError } = require('./errors/NoEntityError');

function plugin(...args) {
    return this.findById(...args)
        .then(result => result || Promise.reject(new NoEntityError('An entity is not found.')));
}

/**
 * @param schema
 * @todo write a test for me
 */
function findByIdOrThrow(schema) {
    /* eslint-disable no-param-reassign */
    schema.statics.findByIdOrThrow = plugin;
}

exports.plugin = plugin;
exports.findByIdOrThrow = findByIdOrThrow;
