const { EntityExistsError } = require('./EntityExistsError');
const { NoEntityError } = require('./NoEntityError');
const { PluginError } = require('./PluginError');

module.exports = {
    EntityExistsError,
    NoEntityError,
    PluginError,
};
