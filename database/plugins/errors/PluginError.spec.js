const { PluginError: main } = require('./PluginError');

describe('main module tests', () => {
    describe('main', () => {
        it('A', () => {
            expect(main).toBeInstanceOf(Function);
        });
    });
});
