const { get, forIn, replace } = require('lodash');

/**
 * @param {string} property
 * @return {string}
 */
function replaced(property) {
    let str = get(this, property, '');
    forIn(this._replacements, (val, key) => {
        str = replace(str, new RegExp(`{{${key}}}`, 'g'), val);
    });
    return str;
}

/**
 * @param {object} replacements
 * @return {*}
 */
function setReplacements(replacements) {
    this._replacements = replacements;
    return this;
}


/**
 * @param {object} options
 * @param {string} options.type
 * @param {object} options.replacements
 * @return {Promise<MailTemplate.model>}
 */
function getOneAsync(options) {
    const type = get(options, 'type', null);
    const replacements = get(options, 'replacements', {});
    return this.findOne({ type }).then((tpl) => {
        if (tpl) tpl.setReplacements(replacements);
        return tpl;
    });
}

/**
 * @param {object} schema
 * @todo write a test for me
 */
function main(schema) {
    schema.methods.replaced = replaced;
    schema.methods.setReplacements = setReplacements;
    schema.statics.getOneAsync = getOneAsync;
}

exports.main = main;
exports.replacements = main;
exports.replaced = replaced;
exports.setReplacements = setReplacements;
exports.getOneAsync = getOneAsync;
