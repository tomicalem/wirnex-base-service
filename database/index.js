const { Mongo } = require('./mongo');

const mongo = new Mongo();

/**
 * @type {{db, mongoose}}
 */
module.exports = mongo.getConnection();
