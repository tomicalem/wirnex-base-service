const bluebird = require('bluebird');
const redis = require('redis');

const log = require('../log').module('cache');

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const redisClient = redis.createClient({
	host: process.env.REDIS_HOST,
	port: process.env.REDIS_PORT,
	retry_strategy,
});

function retry_strategy(options) {
  if (options.error && options.error.code === 'ECONNREFUSED') {
    // End reconnecting on a specific error and flush all commands with a individual error
    return new Error('The server refused the connection');
  }
  if (options.total_retry_time > 1000 * 60 * 60) {
    // End reconnecting after a specific timeout and flush all commands with a individual error
    return new Error('Retry time exhausted');
  }
  if (options.times_connected > 10) {
    // End reconnecting with built in error
    return undefined;
  }
  // reconnect after
  return Math.min(options.attempt * 100, 3000);
}

log.info('Redis client config: ', {host: process.env.REDIS_HOST, port: process.env.REDIS_PORT});

redisClient.on('connect', () => {
	log.info('Redis connected!');
});

let wasShown = false;

redisClient.on('error', (err) => {
	if (!wasShown) {
		wasShown = true;
		log.error('! Redis Error: ', err);
	}
});

module.exports = redisClient;
