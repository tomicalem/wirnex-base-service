# How to use Wirnex Base Service?

1. Create app/lib/wbs-config.js, or copy from demo service.

2. Change requires, like this:
```
const { CrudData } = require('@wirnexteam/wirnex-base-service/data/crud-data');
const buildersApi = require('@wirnexteam/observer-cluster/cluster/builders-api');
```

# How to use Npm registry?
Bump version after GIT commits:
```bash
npm version 1.2.0-beta.1
```

Publish beta version:
```bash
npm publish --tag beta
```

Install beta:
```bash
npm install --save libraryname@beta
```
