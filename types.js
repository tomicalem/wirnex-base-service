/**
 * @typedef {string} ObjectId
 */

/**
 * @typedef {{
 * getMyOne: {path: string, method: string},
 * createMyOne: {path: string, method: string},
 * updateMyOne: {path: string, method: string},
 * deleteMyOne: {path: string, method: string},
 * getMyAll: {path: string, method: string}
 * }} CrudMap
 */

/**
 * @typedef {{
 * getMyOne: string[],
 * createMyOne: string[],
 * updateMyOne: string[],
 * deleteMyOne: string[],
 * getMyAll: string[]
 * }} AclConfig
 */

/**
 * @typedef {{
 * name: string,
 * version: string,
 * }} WbsConfig
 */

/**
 * @type {{
 * rmqExchange: {
 *  maxConcurrentJobs: number,
 *  upstreams: Array,
 *  exchanges: {
 *    name: string,
 *    type: string,
 *    key: string}[],
 *    policies: {
 *      name: string,
 *      pattern: string,
 *      definition: {
 *        "federation-upstream-set": string
 *      },
 *      priority: number,
 *      "apply-to": string
 *    }[],
 *    queuesSubscribe: {
 *      sources: {exchange: {name: string, type: string}, routingKeys: Array}[],
 *      name: string,
 *      key: string}[],
 *      actionsPublish: {
 *        create: {exchange: string, routingKey: string},
 *        update: {exchange: string, routingKey: string},
 *        destroy: {exchange: string, routingKey: string}
*       }
 *    }
 *  }} RmqExchangeConfig
 */

/**
 * @typedef {{
 *  std: RequestPaginationThroughPages,
 *  hp: RequestPaginationHighPerformance,
 * }} PaginatorOptions
 */

/**
 * @typedef {{
 * offset: string,
 * limit: string,
 * page: string,
 * sort: string,
 * }} RequestPaginationThroughPages
 */

/**
 * @typedef {{
 * last_id: string,
 * limit: string
 * }} RequestPaginationHighPerformance
 */
