const _ = require('lodash');

// Wirnex Base Service Config
// Place it into client side
const test = process.env.NODE_ENV === 'test';

const config = test ? {
  name: 'candies-api',
  version: '1.0.0',
  entityNamePlural: 'candies',
  entityNamePluralUcfirst: 'Candies',
} : require('wbs-config');

// Leave it to bring backward support.
// Deprecated, do not rely on this code.
// See WBS configuration example.
// @todo: Remove this code once all services
// @todo: are migrated on new WBS configuration.
const entityNamePlural = `${config.name}`.toLowerCase().replace(/^observer-/, '').replace(/-api$/, '');
const entityNamePluralUcfirst = _.upperFirst(entityNamePlural);

/** @type {WbsConfig} */
module.exports = {
  ...config,
  entityNamePlural: config.entityNamePlural || entityNamePlural,
  entityNamePluralUcfirst: config.entityNamePluralUcfirst || entityNamePluralUcfirst,
};
