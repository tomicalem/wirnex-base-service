const { CrudData } = require('./crud-data');
const { DistributedLock } = require('../models/distributed-lock');

class DistributedLocksData extends CrudData {

  constructor() {
    super('DistributedLock', DistributedLock);
  }

  /**
   * @param {string} documentId
   * @param {string} clientId
   * @returns {Promise<{data: Mongoose.Model}>}
   */
  lockDocument(documentId, clientId = null) {
    return this.createOne({ document_id: documentId, ...(clientId ? {client_id: clientId } : {})});
  }

  /**
   * @param {string} documentId
   * @returns {Bluebird<*>}
   */
  unlockDocument(documentId) {
    return this.Model
      .validateObjectId(documentId)
      .then(() => this.Model.Model.findOneAndRemove({ document_id: documentId }))
      .tap(deleted => deleted ? this.sendModelEvent('deleted', deleted) : null)
      .catch(e => this.Model.handleDbError(e));
  }

  /**
   * @param {string} documentId
   * @returns {Promise<boolean>}
   */
  isLocked(documentId) {
    return this
      .getAll({ document_id: documentId })
      .then(pagination => pagination.data.length > 0);
  }
}

module.exports = { DistributedLocksData };
