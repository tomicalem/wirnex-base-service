jest.mock('../models/model-mongoose.js');
jest.mock('../pagination/pagination');
const { CrudData } = require('./crud-data');
const { ModelMongoose } = require('../models/model-mongoose');

describe('index', () => {
  /** @type {CrudData} */
  let d;
  beforeEach(() => {
    d = new CrudData('test', ModelMongoose);
  });
  describe('class methods', () => {
    test('getAll', async () => {
      await d.getAll('a', 'b', 'c');
      expect(d.Model.getAll).toHaveBeenCalledWith('a', 'b', 'c');
    });

    test('getOne', async () => {
      await d.getOne({ id: 'a' }, { f: 'b' }, { r: 'c' });
      expect(d.Model.getOne).toHaveBeenCalledWith('a', { f: 'b' }, { r: 'c' });
    });

    test('createOne', async () => {
      await d.createOne({ d: 'data' }, { r: 'restrictions' });
      expect(d.Model.createOne).toHaveBeenCalledWith({ d: 'data' }, { r: 'restrictions' });
    });

    test('updateOne', async () => {
      await d.updateOne({ id: 'a' }, { d: 'data' }, { r: 'restrictions' });
      expect(d.Model.updateOne).toHaveBeenCalledWith('a', { d: 'data' }, { r: 'restrictions' });
    });

    test('deleteOne', async () => {
      await d.deleteOne('a', { r: 'restrictions' });
      expect(d.Model.deleteOne).toHaveBeenCalledWith('a', { r: 'restrictions' });
    });

    test('it should create pagination if not provided', async () => {
      await d.getAll('a');
      expect(d.Model.getAll).toHaveBeenCalledWith('a', { limit: 2, offset: 3 }, {});
    });
  });
});
