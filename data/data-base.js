const log = require('../log');

class DataBase {
  /**
   * @param {string} name
   * @param {ModelMongoose} Model
   */
  constructor(name, Model) {
    this.name = name;
    this.Model = Model;
    this.log = log.module(`data/${this.name}`);
  }

  /**
   * Send a model event
   * @protected
   * @param {'created'|'updated'|'deleted'} event
   * @param {*} model
   * @returns {Promise<void>}
   */
  sendModelEvent(event, model) {
    this.log.trace('Model Event: ', this.name, event, model);
    return Promise.resolve()
      .tap(() => rabbit.publishToExchange(model, `${event}`))
      .tapCatch(err => this.log.error('Cannot send RMQ message', err));
  }
}

module.exports = { DataBase };
