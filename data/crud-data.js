const { DataBase } = require('./data-base');
const { Pagination } = require('../pagination');

/**
 * @class CrudData
 * @property {ModelMongoose} Model
 */
class CrudData extends DataBase {
  /**
   * Get all
   * @public
   * @param {Object<*>} filter
   * @param {PaginationBase} pagination
   * @param {Object} restrictions
   * @return {Promise<{ data: Mongoose.Model[], pagination: * }>}
   */
  getAll(filter, pagination = null, restrictions = {}) {
    if (!pagination) {
      pagination = Pagination.make();
    }
    return this.Model.getAll(filter, pagination, restrictions);
  }

  /**
   * Get one
   * @public
   * @param {Object} params
   * @param {Object} filter
   * @param {Object | null} restrictions
   * @return {Promise<{ data: Mongoose.Model }>}
   */
  getOne(params, filter = {}, restrictions = {}) {
    const id = params.id;
    return this.Model.getOne(id, filter, restrictions);
  }

  /**
   * Create one
   * @public
   * @param {*} data
   * @param {Object} [restrictions]
   * @return {Promise<{data: Mongoose.Model}>}
   */
  createOne(data, restrictions = {}) {
    return Promise.resolve()
      .then(() => this.Model.createOne(data, restrictions))
  }

  /**
   * Update one
   * @public
   * @param {Object} params
   * @param {*} data
   * @param {Object | null} [restrictions]
   * @return {Promise<{data: Mongoose.Model}>}
   */
  updateOne(params, data, restrictions = {}) {
    const id = params.id;
    return Promise.resolve()
      .then(() => this.Model.updateOne(id, data, restrictions))
  }

  /**
   * Delete one
   * @public
   * @param {ObjectId} id
   * @param {Object | null} [restrictions]
   * @return {Promise<{ data: { success: boolean } }>}
   */
  deleteOne(id, restrictions = {}) {
    return Promise.resolve()
      .then(() => this.Model.deleteOne(id, restrictions))
  }

  /**
   * Aggregate
   * @param {{ match: { user_id: string }, group: {  } }} filter
   * ?match[project_id][0]=5badd2c326908101f33029b7&group[_id]=$project_id&group[count.$sum]=1&match[project_id][1]=5ccdfc0d89a37d0010357f46
   *   filter: {
   *     match: {
   *       project_id: [ '5badd2c326908101f33029b7', '5ccdfc0d89a37d0010357f46' ] },
   *     group: {
   *       _id: '$project_id', 'count.$sum': '1'
   *     }
   *   }
   * Output
   * { "data": [{ "_id": "5ccdfc0d89a37d0010357f46", "count": 1 },
   *     { "_id": "5badd2c326908101f33029b7", "count": 6 }] }
   *
   * @return {Promise.<T>}
   */
  aggregate(filter, pagination = null, restrictions = {}) {
    if (!pagination) {
      pagination = Pagination.make(filter);
    }
    return this.Model.aggregate(filter, pagination, restrictions);
  }
}

module.exports = { CrudData };
